#include "BluetoothAPI.h"


bool BluetoothAPI::setup(String deviceName){
    return SerialBT.begin(deviceName);
}

void BluetoothAPI::update(){
    if (Serial.available()){
        while (Serial.available()) {
            char c = Serial.read();
            SerialBT.write(c);
            Serial.write(c);
        }
        // Serial.flush();
    }

    if (SerialBT.available()){
        while (SerialBT.available()) {
            Serial.write(SerialBT.read());
        }
        // Serial.flush();
        // Serial.println(".");
    }
}
