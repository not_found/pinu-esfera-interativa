#ifndef NEOPIXEL_LEDSERVICES_H
#define NEOPIXEL_LEDSERVICES_H

#include "LedServices.h"

#include <NeoPixelBus.h>

#ifndef NEOPIXEL_DEFAULT_METHOD
  #if defined(ESP32) || defined(ARDUINO_ARCH_ESP32)
    #define NEOPIXEL_DEFAULT_METHOD NeoEsp32Rmt0800KbpsMethod
  #else
    #define NEOPIXEL_DEFAULT_METHOD Neo800KbpsMethod
  #endif
#endif

template <
  typename NeoFeature    =NeoGrbFeature,
  typename NeoPixelMethod=NEOPIXEL_DEFAULT_METHOD
>
class NeoPixelLedServices: public LedServices {
protected:
  NeoPixelBus<NeoFeature, NeoPixelMethod> ledStrip;

public:
  NeoPixelLedServices (uint16_t pixelCount, uint8_t pixelPin)
    : ledStrip(pixelCount, pixelPin)
  {}

  void begin() override{
      ledStrip.Begin();
  }

  void changeColor(ColorDef color) override{
    RgbColor rgbColor(color.r, color.g, color.b);

    ledStrip.ClearTo(rgbColor);
    ledStrip.Show();
  }
};

#endif
