#include "BuzzerSoundServices.h"

#include "ArduinoFramework.h"

BuzzerSoundServices::BuzzerSoundServices(int buzzerPin, int channel)
  : buzzerPin(buzzerPin)
  , buzzerChannel(channel)
  , playing(false)
  , paused(false)
  , currentToneIdx(0)
{}

void BuzzerSoundServices::begin(){
  ledcSetup(buzzerChannel, 0 /* frequency */ , 8 /* resolution*/);
  ledcAttachPin(buzzerPin, buzzerChannel);
}
void BuzzerSoundServices::update(){
  if (!isPlaying()) {
    return;
  }

  if (counter.completed()) {
    if (!isOnPause()) {
      pause();
    }
    else{
      nextTone();
    }
  }
}

void BuzzerSoundServices::play(const Melody & melody){
  currentMelody = melody;

  playing = true;
  currentToneIdx = 0;
  startTone(currentTone());
}

void BuzzerSoundServices::startTone(const ToneDef & tone){
  ledcWriteTone(buzzerChannel, tone.frequency);
  counter.resetTimer(tone.duration);
}

void BuzzerSoundServices::nextTone(){
  ++currentToneIdx;

  if (currentToneIdx < currentMelody.size()) {
    startTone(currentTone());
  }
  else{
    stop();
  }
}

void BuzzerSoundServices::pause(){
  off();

  counter.resetTimer(currentTone().pause);
  paused = true;
}

void BuzzerSoundServices::off(){
  ledcWrite(buzzerChannel, 0);
}

bool BuzzerSoundServices::isPlaying(){
  return playing;
}

bool BuzzerSoundServices::isOnPause(){
  return isPlaying() && paused;
}

void BuzzerSoundServices::stop(){
  off();

  playing = false;
  // currentToneIdx = 0;
}

int BuzzerSoundServices::currentToneIndex(){
  return currentToneIdx;
}

const ToneDef & BuzzerSoundServices::currentTone() const{
  return currentMelody[currentToneIdx];
}
