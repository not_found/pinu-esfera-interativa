#ifndef BUZZER_SOUND_SERVICES
#define BUZZER_SOUND_SERVICES

#include "SoundServices.h"

#include "TimeCounter.h"

class BuzzerSoundServices: public SoundServices {
public:
  using Melody = SoundServices::Melody;

public:
  BuzzerSoundServices(int buzzerPin, int channel);

  void begin() override;
  void update() override;

  void play(const Melody & melody) override;
  bool isPlaying() override;
  bool isOnPause() override;
  void stop() override;

  int currentToneIndex() override;

protected:
  void startTone(const ToneDef & tone);
  void nextTone();
  void pause();
  void off();

  const ToneDef & currentTone() const;

protected:
  int buzzerPin;
  int buzzerChannel;
  bool playing;
  bool paused;

  int currentToneIdx;
  Melody currentMelody;
  TimeCounter counter;
};

#endif
