#include "SphereServices.h"

SphereServices::SphereServices(SphereServicesConfig config)
  : config(config)
{};

void SphereServices::begin(){
  beginService(led());
  beginService(vibration());
  beginService(sound());
  beginService(orientation());
  beginService(pressure());
}

void SphereServices::update(){
  updateService(led());
  updateService(vibration());
  updateService(sound());
  updateService(orientation());
  updateService(pressure());
}

void SphereServices::beginService(Service * service){
  if (service) {
    service->begin();
  }
}

void SphereServices::updateService(Service * service){
  if (service != nullptr) {
    service->update();
  }
}
