#ifndef PRESSURE_SERVICES_H
#define PRESSURE_SERVICES_H

#include "Service.h"

#include <functional>
#include <unordered_map>

#include "pressure/Event.h"
#include "pressure/EventListener.h"
#include "pressure/SensorDescriptions.h"

using PressureEvent = pressure::Event;
using PressureListener = pressure::EventListener;

class PressureServices : public Service {
public:
    using SensorDesc = pressure::SensorDescription;

public:
    virtual ~PressureServices (){};

    virtual const SensorDesc &         getSensorDescription(
                                                pressure::SensorId sensor)=0;
    virtual const SensorDescriptions & getSensorDescriptions()=0;
    virtual void listenPressureEvents(PressureListener listener)=0;
    //To-Do: allow specify what listener
    virtual void removeListener()=0;
};

#endif
