#ifndef SPHERE_SERVICES_H
#define SPHERE_SERVICES_H

#include "ArduinoFramework.h"

#include "LedServices.h"
#include "VibrationServices.h"
#include "SoundServices.h"
#include "OrientationServices.h"
#include "PressureServices.h"

class SphereServicesConfig{
public:
  SphereServicesConfig(
    LedServices         * led         = nullptr,
    VibrationServices   * vibration   = nullptr,
    SoundServices       * sound       = nullptr,
    OrientationServices * orientation = nullptr,
    PressureServices    * pressure    = nullptr)
    : led(led)
    , vibration(vibration)
    , sound(sound)
    , orientation(orientation)
    , pressure(pressure)
  {}

public:
  LedServices         * led;
  VibrationServices   * vibration;
  SoundServices       * sound;
  OrientationServices * orientation;
  PressureServices    * pressure;
};

class SphereServices: public Service{
public:
  SphereServices(SphereServicesConfig config={});

  // void listenPressChanges(config, listener);

  void begin() override;
  void update() override;

public:
  LedServices         * led()        { return config.led; }
  VibrationServices   * vibration()  { return config.vibration;}
  SoundServices       * sound()      { return config.sound; }
  OrientationServices * orientation(){ return config.orientation;}
  PressureServices    * pressure()   { return config.pressure;}

protected:
  void beginService(Service * service);
  void updateService(Service * service);

protected:
  SphereServicesConfig config;
};

#endif
