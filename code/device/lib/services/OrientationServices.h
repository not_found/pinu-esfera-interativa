#ifndef ORIENTATION_SERVICES_H
#define ORIENTATION_SERVICES_H

#include "Service.h"

#include "3dmath/Vector.hpp"
#include "3dmath/Vector3D.hpp"
#include "OrientationFormat.h"

#include <functional>


using OrientationListener = std::function<void(const Vector<float> & values)>;
using GravityVector = Vector3D<float>;

class ListenOrientationConfig {
public:
    ListenOrientationConfig (OrientationFormat format, int timeInterval=0)
        : format(format)
        , timeInterval(timeInterval)
    {}

    bool operator==(const ListenOrientationConfig & other) const{
        return format == other.format
            && timeInterval == other.timeInterval;
    }

public:
    OrientationFormat format;
    int timeInterval;
};

class OrientationServices : public Service{
public:
  virtual void calibrate()=0;
  virtual void calibrate(const GravityVector & gravity)=0;

  virtual void listenOrientation(OrientationListener listener, ListenOrientationConfig config)=0;
  //To-Do: add listener id
  virtual void removeListener()=0;
};


#endif
