#ifndef PRESSURE_EVENT_TYPE_H
#define PRESSURE_EVENT_TYPE_H

namespace pressure {

class EventType{
public:
  enum Value{
      None,
      Press,
      PressContinue,
      Release
  };

  EventType() = default;
  constexpr EventType(Value value) : value(value) { }

  operator Value() const { return value; }

public:
    const char * c_str() const{
        switch (value){
        case None:          return "None";
        case Press:         return "Press";
        case PressContinue: return "PressContinue";
        case Release:       return "Release";
        default:            return "Unknown";
        }
    }

protected:
  Value value;
};

} /* pressure */
#endif
