#ifndef PRESSURE_EVENT_H
#define PRESSURE_EVENT_H

#include "SensorId.h"
#include "EventType.h"

namespace pressure {

    class Event {
    public:
        Event (SensorId sensorId, EventType evtType, float percent, long raw)
            : sensorId(sensorId)
            , evtType(evtType)
            , percentValue(percent)
            , rawValue(raw)
        {}

    public:
        SensorId sensorId;
        EventType evtType;
        float percentValue;
        long rawValue;
    };

} /* pressure */

#endif
