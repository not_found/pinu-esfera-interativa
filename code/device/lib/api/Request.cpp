#include "Request.h"

using namespace api;

Request::Request()
    : Request(0, Methods::Unknown)
{}


Request::Request(Methods method, String target, RequestArguments arguments)
    : Request(0, method, std::move(target), std::move(arguments))
{}

Request::Request(
    long id,
    Methods method,
    String target,
    RequestArguments arguments)
    : id(id)
    , method(method)
    , target(std::move(target))
    , arguments(std::move(arguments))
{}

String Request::findArgument(const String & key){
    auto found = arguments.find(key);

    return found == arguments.end() ? String{} : found->second;
}
