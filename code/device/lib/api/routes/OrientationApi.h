#ifndef ORIENTATION_API_H
#define ORIENTATION_API_H

#include "Api.h"
#include "SphereServices.h"

namespace api {
    class OrientationApi: public Api{
    public:
        using Api::Api; //importing constructor

    public:
        virtual void registerRoutes(Router & router){
            addRoute({"/orientation/calibrate", Methods::Post},
                        this, &OrientationApi::calibrate, router);
            addRoute({"/orientation", Methods::Listen},
                        this, &OrientationApi::listenOrientation, router);
            addRoute({"/orientation", Methods::UnListen},
                        this, &OrientationApi::removeListener, router);
        }

        void onDisconnected() override{
            if (services() && services()->orientation()){
                //FIXME: should check if current listen is this instance
                services()->orientation()->removeListener();
            }
        }
    public:
        void calibrate(Request & req, Response & res){
            if (!checkServices(res)){
                return;
            }

            GravityVector gravity;
            auto found = req.arguments.find("gravity");

            if (found == req.arguments.end()){
                services()->orientation()->calibrate();
            }
            else if(parser().parseVector(found->second, gravity)){
                services()->orientation()->calibrate(gravity);
            }
            else{//failed to parse gravity
                res.status(Response::Status::BadFormat);
            }

        }

        void listenOrientation(Request & req, Response & res){
            if (!checkServices(res)){
                return;
            }

            String formatStr = req.findArgument("format");
            OrientationFormat format = OrientationFormat::Quaternion;

            String intervalStr = req.findArgument("interval");
            int interval = intervalStr.toInt();

            if ((formatStr.length() > 0
                && !parser().parseOrientationFormat(formatStr, format))
                || (intervalStr.length() > 0 && interval == 0 && intervalStr != "0"))
            {
                res.status(Response::Status::BadFormat);
                return;
            }

            auto reqId = req.id;

            services()->orientation()->listenOrientation(
                [this, reqId, format](const Vector<float> & vec){
                    sendNotification({
                        reqId, Response::Status::Notification, "",
                        {{"format", format.c_str()}},
                        vec.toString()
                    });
                }, {format, interval});
        }

        void removeListener(Request & req, Response & res){
            if (!checkServices(res)){
                return;
            }

            services()->orientation()->removeListener();
        }

    protected:
        bool checkServices(Response & res){
            if(!services() || !services()->orientation()){
                res.status(Response::Status::ServerError);
                return false;
            }

            return true;
        }
    };
} /* api */

#endif
