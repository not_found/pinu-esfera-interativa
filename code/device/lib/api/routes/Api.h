#ifndef ROUTE_HANDLER_H
#define ROUTE_HANDLER_H

#include "ApiRouter.h"
#include "SphereServices.h"

#include "ApiParser.h"
#include "ApiNotifier.h"

namespace api {
    class Api {
    public:
        Api(SphereServices * services, Notifier * notifier=nullptr)
            :_services(services)
            ,_notifier(notifier)
        {}

        virtual ~Api (){}

        virtual void registerRoutes(Router & router)=0;
        virtual void onDisconnected(){}

    public:
        inline SphereServices * services(){ return _services;}
        inline SphereServices * services(SphereServices * services){
            return this->_services = services;
        }
        inline Notifier* notifier(){
            return this->_notifier;
        }
        inline Notifier* notifier(Notifier * notifier){
            return this->_notifier = notifier;
        }

        inline Parser & parser(){ return _parser;}

    protected:
        template<typename T,
                 typename RouteMethod = void (T::*)(Request &, Response &)>
        void addRoute(RoutePath path, T * self, RouteMethod m, Router & router){
            auto route = std::bind(m, self,
                                    std::placeholders::_1, std::placeholders::_2);
            router.putRoute(path, route);;
        }

        void sendNotification(const Response & res){
            if (!_notifier){
                return; //To-do: notify error (?)
            }

            _notifier->notify(res);
        }

        bool requireServices(std::vector<Service *> services, Response & res){
            for (auto * s : services){
                if (!s){
                    res.status(Response::Status::ServerError);
                    return false;
                }
            }

            return true;
        }
    protected:
        SphereServices * _services;
        Notifier * _notifier;
        Parser _parser;
    };
} /* api */

#endif
