#ifndef LED_API_H
#define LED_API_H

#include "Api.h"
#include "SphereServices.h"

namespace api {
    class LedApi: public Api{
    public:
        using Api::Api; //importing constructor

    public:
        virtual void registerRoutes(Router & router){
            addRoute({"/led/color", Methods::Put} , this, &LedApi::putColor, router);
            addRoute({"/led/off"  , Methods::Post}, this, &LedApi::ledOff, router);
        }

    public://routes
        void putColor(Request & req, Response & res){
            ColorDef color;

            if (!ColorDef::parse(req.arguments["color"], color)){
                res.status(Response::Status::BadRequest)
                   .description("Missing a valid 'color' argument");

                return;
            }

            if (services()->led()){
                services()->led()->changeColor(color);
            }
            //To-do: else error
        }

        void ledOff(Request & req, Response & res){
            if (services()->led()){
                services()->led()->off();
            }
        }

    };
} /* api */

#endif
