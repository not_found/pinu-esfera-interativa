#ifndef API_ROUTER_H
#define API_ROUTER_H

#include "ArduinoFramework.h"

#include "ApiRoute.h"
#include "RoutePath.h"

#include <functional>
#include <map>

namespace api {

class Router {
public:
    // using RouteMap = std::map<String, Route>;
    using RouteMap = std::map<RoutePath, Route>;

public:
    template<typename T>
    static Route makeRoute(void (T::*f)(Request & req, Response &res), T * ptr){
        return std::bind(f, ptr, std::placeholders::_1, std::placeholders::_2);
    }

public:
    virtual ~Router (){}

    void clear();

    void route(Request & req, Response & res);
    void putRoute(const RoutePath & path, Route route);
    void addRoutes(const RouteMap & routes);

    void defaultRoute(Route route);

    inline
    const RouteMap & routes() const{ return routeMap;}

protected:
    RouteMap routeMap;
    Route _defaultRoute;
};

} /* api */

#endif
