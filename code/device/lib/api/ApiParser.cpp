#include "ApiParser.h"

#define ARDUINOJSON_ENABLE_ARDUINO_STRING 1
#include "ArduinoJson.h"

#include "StringStream.h"
#include "StreamOperators.h"

#include "log.h"

#include <functional> //reference_wrapper

using namespace api;

String Parser::serializeResponse(const Response & r){
    String out;

    bool hasDescription = (r.description().length() > 0);

    out << r.id()     << ' '
        << r.status() << ' '
        << (hasDescription? r.description().c_str() : r.status().c_str())
        << '\n';

    for (const auto & arg : r.args()){
        out << arg.first << ": " << arg.second << '\n';
    }

    if (r.data().length() > 0){
        out << "len: " << r.data().length() << "\n\n";
        out << r.data();
    }
    else{
        out << '\n';
    }


    return std::move(out);
}

String Parser::serializePressureSensor(const pressure::SensorDescription & sensor) const{
    StaticJsonDocument<256> jsonDoc;
    auto obj = jsonDoc.to<JsonObject>();

    obj["id"] = sensor.id();
    obj["name"] = sensor.name().c_str();

    if (sensor.description().length() > 0){
        obj["desc"] = sensor.description().c_str();
    }

    String out;
    serializeJson(obj, out);

    return std::move(out);
}

String Parser::serializePressureSensors(const SensorDescriptions & sensors) const{
    String out{"["};

    bool first=true;
    for (const auto & pair: sensors){ //To-Do: limit allocated memory
        if (!first){
            out << ",";
        }
        out << serializePressureSensor(pair.second);

        first = false;
    }
    out << "]";

    return std::move(out);
}


String Parser::serializePressureEvent(const pressure::Event & evt) const{
    StaticJsonDocument<JSON_OBJECT_SIZE(4)> jsonDoc;
    auto obj = jsonDoc.to<JsonObject>();

    obj["sensor"]   = evt.sensorId;
    obj["type"]     = evt.evtType.c_str();
    obj["relative"] = evt.percentValue;
    obj["raw"]      = evt.rawValue;

    String out;
    serializeJson(obj, out);

    return std::move(out);
}

String Parser::serializeRoutes(const Router::RouteMap & routes){
    StaticJsonDocument<1024> jsonDoc;
    JsonObject obj = jsonDoc.to<JsonObject>();

    for(const auto & pair: routes){
        const auto & routePath = pair.first;

        auto member = obj.getOrAddMember(routePath.path);
        JsonArray routeMember = member.is<JsonArray>() ? member.as<JsonArray>() : member.to<JsonArray>();
        routeMember.add(routePath.method.c_str());
    }

    String out;
    serializeJson(obj, out);
    return std::move(out);
}

bool Parser::parseVector(const String & in, Vector3D<float> & out){
    StaticJsonDocument<JSON_ARRAY_SIZE(3)> jsonDoc;
    auto error = deserializeJson(jsonDoc, in);

    if (error || jsonDoc.isNull() || !jsonDoc.is<JsonArray>()){
        return false;
    }

    JsonArray array = jsonDoc.as<JsonArray>();
    if (array.size() != 3){
        return false;
    }

    for (std::size_t i = 0; i < 3; ++i){
        auto v = array[i];

        if (!v.is<float>()){
            return false;
        }

        out.data()[i] = v.as<float>();
    }

    return true;
}


bool Parser::parseOrientationFormat(const String & in, OrientationFormat & out){
    String formatStr{in};
    formatStr.trim();
    formatStr.toLowerCase();

    if (formatStr == "euler"){
        out = OrientationFormat::Euler;
    }
    else if (formatStr == "quaternion"){
        out = OrientationFormat::Quaternion;
    }
    else if (formatStr == "yawpitchrow"){
        out = OrientationFormat::YawPitchRow;
    }
    else{
        return false;
    }

    return true;
}
