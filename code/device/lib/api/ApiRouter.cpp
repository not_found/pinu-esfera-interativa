#include "ApiRouter.h"

using namespace api;

void Router::clear(){
    routeMap.clear();
}

void Router::route(Request & req, Response & res){
    RoutePath path{req.target, req.method};
    auto route = routeMap[path];

    if (!route){
        path.method = Methods::Any;
        route = routeMap[path];
    }

    if (route){
        route(req, res);
    }
    else if (_defaultRoute){
        _defaultRoute(req, res);
    }
}


void Router::addRoutes(const RouteMap & routes){
    for (const auto & pair: routes){
        putRoute(pair.first, pair.second);
    }
}

void Router::putRoute(const RoutePath & path, Route route){
    this->routeMap[path] = route;
}

void Router::defaultRoute(Route route){
    this->_defaultRoute = route;
}
