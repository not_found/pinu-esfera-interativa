#ifndef API_REQUEST_H
#define API_REQUEST_H

#include "ArduinoFramework.h"

#include "Methods.h"
#include "RequestArguments.h"

namespace api{

class Request {
public:
    Request();
    Request(Methods method, String target={}, RequestArguments arguments={});
    Request(long id, Methods method, String target={}, RequestArguments arguments={});

public:
    String findArgument(const String & key);

public:
    long id;
    Methods method;
    String target;
    RequestArguments arguments;
};

}//namespace

#endif
