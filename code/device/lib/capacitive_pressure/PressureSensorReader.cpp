#include "PressureSensorReader.h"

PressureSensorReader::PressureSensorReader (
    CapReader * reader,
    int medianWindowSize,
    int averageWindowSize)
    : reader(reader)
    , runningMedian(medianWindowSize)
    , runningAverage(averageWindowSize)
{}

bool PressureSensorReader::update(){
    if (!reader){
        return false;
    }

    return runningMedian.update(reader->read())
        && runningAverage.update(runningMedian.current())
        && rangeFilter.update(runningAverage.current())
    ;
}
long PressureSensorReader::read(){
    while(!update()){}

    return current();
}

long PressureSensorReader::current(){
    return rangeFilter.current();
}
float PressureSensorReader::relative(){
    return rangeFilter.relative();
}
