#include "CapPressureServices.h"

#include "log.h"

#include <functional>

using namespace pressure;
using namespace std::placeholders;

using CapService     = CapPressureServices;
using MuxReaderArray = CapService::MuxReaderArray;
using SensorsArray   = CapService::SensorsArray;

CapService::CapPressureServices (Config c)
    : mux(c.muxPins()[0],
          c.muxPins()[1],
          c.muxPins()[2],
          c.muxPins()[3])
    , capReader(c.capSenseRcvPin(), c.capSenseSendPin())
    , sensorDescriptions(std::move(c.sensors()))
    , numSensors(sensorDescriptions.size())
    , muxReaders(std::move(makeMuxReaders(
        sensorDescriptions, &mux, &capReader)))
    , pressureSensors(std::move(makeSensors(muxReaders, numSensors, c)))
{
}

MuxReaderArray CapService::makeMuxReaders(
    const
    SensorDescriptions & sensors,
    Multiplexer        * mux,
    CapReader          * reader)
{
    MuxReaderArray muxArray;

    int count=0;
    for(const auto & sPair: sensors){
        auto channel = sPair.first; //sensor id

        muxArray[count] = MuxReader(channel, mux, reader);

        ++count;
    }

    return std::move(muxArray);
}

SensorsArray CapService::makeSensors(
    MuxReaderArray & readers,
    int numSensors,
    const Config & c)
{
    SensorsArray sensors;

    for (int i = 0; i < numSensors; ++i){
        auto * reader = &readers[i];
        SensorId id = reader->getChannel();

        sensors[i] = PressureSensor(
            id,
            reader,
            c.medianWindowSize(),
            c.averageWindowSize(),
            c.releaseThreshold(),
            c.pressThreshold()
        );
    }

    return std::move(sensors);
}

void CapService::begin(){
    configSensors();
}

void CapService::configSensors(){
    auto listener = std::bind(&CapService::onSensorEvent, this, _1);
    for (int i = 0; i < numSensors; ++i){
        auto & sensor = this->pressureSensors[i];
        sensor.listenPressureEvents(listener);
    }
}

void CapService::update(){
    for (int i = 0; i < numSensors; ++i){
        pressureSensors[i].update();
    }
}

void CapService::onSensorEvent(const pressure::Event & evt){
    if (listener){
        listener(evt);
    }
}

const SensorDescription &
    CapService::getSensorDescription(SensorId sensorId)
{
    return sensorDescriptions[sensorId];
}

const SensorDescriptions & CapService::getSensorDescriptions(){
    return sensorDescriptions;
}

void CapService::listenPressureEvents(PressureListener listener){
    this->listener = listener;
}

void CapService::removeListener(){
    this->listener = {};
}
