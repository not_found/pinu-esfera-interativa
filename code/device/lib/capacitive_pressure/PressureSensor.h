#ifndef PRESSURE_SENSOR_H
#define PRESSURE_SENSOR_H

#include "PressureSensorReader.h"
#include "PressureEventDetector.h"
#include "pressure/EventListener.h"

class PressureSensor{
public:
    PressureSensor();

    PressureSensor (
        pressure::SensorId id,
        CapReader * reader,
        int medianWindowSize  = MEDIAN_WINDOW_SIZE,
        int averageWindowSize = AVERAGE_WINDOW_SIZE,

        float releaseThreshold = PRESSURE_RELEASE_THRESHOLD,
        float pressThreshold   = PRESSURE_PRESS_THRESHOLD,
        int debounceThreshold  = PRESSURE_DEBOUNCE_THRESHOLD
    );

    inline
    pressure::SensorId id() const{ return detector.id(); }

    bool update();
    void listenPressureEvents(pressure::EventListener listener);

public:
    PressureSensorReader reader;
    PressureEventDetector detector;
};

#endif
