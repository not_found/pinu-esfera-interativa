#ifndef CAP_PRESSURE_SERVICES_H
#define CAP_PRESSURE_SERVICES_H

#include "PressureServices.h"

#include "PressureSensor.h"
#include "MuxReader.h"
#include "reader/CapSenseReader.h"

#include "CapPressureServicesConfig.h"

class CapPressureServices : public PressureServices{
public:
    using Config = CapPressureServicesConfig;
    using MuxReaderArray = std::array<MuxReader, MULTIPLEXER_CHANNELS>;
    using SensorsArray = std::array<PressureSensor, MULTIPLEXER_CHANNELS>;
public:
    CapPressureServices (Config config);

    void begin() override;
    void update() override;

    const SensorDesc & getSensorDescription(pressure::SensorId sensor) override;
    const SensorDescriptions & getSensorDescriptions() override;
    void listenPressureEvents(PressureListener listener) override;
    void removeListener() override;

protected:
    void configSensors();

    void onSensorEvent(const pressure::Event & evt);

protected:
    static MuxReaderArray makeMuxReaders(
        const
        SensorDescriptions & sensors,
        Multiplexer        * mux,
        CapReader          * reader);

    static SensorsArray makeSensors(
        MuxReaderArray & readers,
        int numSensors,
        const Config & configs);

protected:
    Multiplexer mux;
    CapSenseReader capReader;
    SensorDescriptions sensorDescriptions;
    std::size_t numSensors;
    MuxReaderArray muxReaders;
    SensorsArray pressureSensors;

    pressure::EventListener listener;
};

#endif
