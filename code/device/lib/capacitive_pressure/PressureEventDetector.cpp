#include "PressureEventDetector.h"

#include "log.h"

namespace pressure{

EventDetector::EventDetector(
    SensorId id,
    float releaseThreshold, float pressThreshold,
    int debounceThreshold
    )
    : _id(id)
    , levelTrigger(releaseThreshold, pressThreshold)
    , debounce(debounceThreshold)
    , currentState(EventType::None)
{}

void EventDetector::reset(){
    debounce.reset();
    debounce.state(false);
    levelTrigger.state(false);
}

bool EventDetector::update(float relative, long raw){
    levelTrigger.update(relative);

    if (!levelTrigger.isMiddle()){
        auto changed = debounce.update(levelTrigger.state());

        // log_info("(%f, %ld) - levelTrigger: %d, debounce: %d, changed: %d\n",
        //     relative, raw,
        //     levelTrigger.state(), debounce.state(), changed
        // );

        if(changed){
            changeState(debounce.state(), relative, raw);
            return true;
        }
    }

    return false;
}

void EventDetector::changeState(bool pressed, float relative, long raw){
    currentState = (pressed ? EventType::Press : EventType::Release);
    if (listener){
        listener(Event{
            id(),
            currentState,
            relative,
            raw
        });
    }
}

void EventDetector::onPressureEvent(EventListener listener){
    this->listener = listener;
}

EventType EventDetector::state(){
    return currentState;
}

int EventDetector::debounceThreshold(int threshold){
    return debounce.countThreshold(threshold);
}
float EventDetector::lowerLevelThreshold(float threshold){
    return levelTrigger.lowerThreshold(threshold);
}
float EventDetector::upperLevelThreshold(float threshold){
    return levelTrigger.upperThreshold(threshold);
}

}//namespace
