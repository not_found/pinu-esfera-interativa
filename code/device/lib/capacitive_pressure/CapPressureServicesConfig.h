#ifndef CAP_PRESSURE_SERVICES_CONFIG_H
#define CAP_PRESSURE_SERVICES_CONFIG_H

#include "PressureServices.h"

#include <array>

namespace pressure{
namespace services_config {

class Config {
public:
    using Self = Config;

public:
    Config (SensorDescriptions sensors={});

public: //setters
    Self & muxPins(std::array<int, 4> muxPins);
    Self & capSensePins(int rcvPin, int sendPin);
    Self & medianWindowSize(std::size_t size);
    Self & averageWindowSize(std::size_t size);
    Self & pressThreshold(float threshold);
    Self & releaseThreshold(float threshold);

    Self & sensors(SensorDescriptions descriptions);
    Self & addSensor(pressure::SensorDescription sensor);

public://getters
    inline
    const std::array<int, 4> & muxPins() const{ return _muxPins;}

    inline
    int capSenseRcvPin()    const{ return _capSenseRcvPin; }
    inline
    int capSenseSendPin()   const{ return _capSenseSendPin; }
    inline
    int medianWindowSize()  const{ return _medianWindowSize; }
    inline
    int averageWindowSize() const{ return _averageWindowSize; }

    inline
    float pressThreshold()    const{ return sensorPressThreshold;}
    inline
    float releaseThreshold()  const{ return sensorReleaseThreshold; }

    const SensorDescriptions & sensors() const { return sensorDescriptions;}

public:
    inline Self & self(){ return *this; }

protected:
    std::array<int, 4> _muxPins;
    int _capSenseRcvPin, _capSenseSendPin;
    SensorDescriptions sensorDescriptions;
    int _medianWindowSize, _averageWindowSize;
    float sensorReleaseThreshold, sensorPressThreshold;
};

}//namespace
}//namespace

using CapPressureServicesConfig = pressure::services_config::Config;

#endif
