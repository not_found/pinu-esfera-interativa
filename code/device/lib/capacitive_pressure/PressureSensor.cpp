#include "PressureSensor.h"

using namespace pressure;

PressureSensor::PressureSensor()
    : PressureSensor(0, nullptr, 0, 0)
{}

PressureSensor::PressureSensor (
    pressure::SensorId id,
    CapReader * reader,
    int medianWindowSize,
    int averageWindowSize,

    float releaseThreshold,
    float pressThreshold,
    int debounceThreshold
)
    : reader(reader, medianWindowSize, averageWindowSize)
    , detector(id, releaseThreshold, pressThreshold, debounceThreshold)
{}

bool PressureSensor::update(){
    return reader.update()
        && detector.update(reader.relative(), reader.current());
}

void PressureSensor::listenPressureEvents(EventListener listener){
    detector.onPressureEvent(listener);
}
