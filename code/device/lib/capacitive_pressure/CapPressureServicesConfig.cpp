#include "CapPressureServicesConfig.h"

#include "BoardConfig.h"
#include "BoardPinout.h"

using pressure::services_config::Config;

Config::Config (SensorDescriptions sensors)
    : _muxPins{MUX_S0_PIN, MUX_S1_PIN, MUX_S2_PIN, MUX_S3_PIN}
    , _capSenseRcvPin(CAPSENSE_RCV_PIN)
    , _capSenseSendPin(CAPSENSE_SND_PIN)
    , sensorDescriptions(std::move(sensors))
    , _medianWindowSize(MEDIAN_WINDOW_SIZE)
    , _averageWindowSize(AVERAGE_WINDOW_SIZE)
    , sensorReleaseThreshold(PRESSURE_RELEASE_THRESHOLD)
    , sensorPressThreshold(PRESSURE_PRESS_THRESHOLD)
{}

Config & Config::muxPins(std::array<int, 4> muxPins){
    this->_muxPins = std::move(muxPins);

    return self();
}
Config & Config::capSensePins(int rcvPin, int sendPin){
    this->_capSenseRcvPin = rcvPin;
    this->_capSenseSendPin = sendPin;
    return self();
}
Config & Config::medianWindowSize(std::size_t size){
    this->_medianWindowSize = size;
    return self();
}
Config & Config::averageWindowSize(std::size_t size){
    this->_averageWindowSize = size;
    return self();
}
Config & Config::pressThreshold(float threshold){
    this->sensorPressThreshold = threshold;
    return self();
}
Config & Config::releaseThreshold(float threshold){
    this->sensorReleaseThreshold = threshold;
    return self();
}

Config & Config::sensors(SensorDescriptions descriptions){
    this->sensorDescriptions = std::move(descriptions);
    return self();
}
Config & Config::addSensor(pressure::SensorDescription sensor){
    this->sensorDescriptions[sensor.id()] = std::move(sensor);
    return self();
}
