#ifndef PRESSURE_SENSOR_READER_H
#define PRESSURE_SENSOR_READER_H

#include "BoardConfig.h"

#include "FastRunningMedian.h"
#include "RunningAverage.h"
#include "RangeFilter.h"

#include "reader/CapReader.h"

class PressureSensorReader: public CapReader{
public:
    PressureSensorReader (
        CapReader * reader = nullptr,
        int medianWindowSize  = MEDIAN_WINDOW_SIZE,
        int averageWindowSize = AVERAGE_WINDOW_SIZE);

    virtual bool update();
    long read() override;
    long current();
    float relative();

public:
    CapReader * reader;
    FastRunningMedian<long> runningMedian;
    RunningAverage runningAverage;
    RangeFilter<long> rangeFilter;
};

#endif
