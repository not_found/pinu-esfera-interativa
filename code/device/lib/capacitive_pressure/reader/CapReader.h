#ifndef CAP_READER_H
#define CAP_READER_H

class CapReader {
public:
    virtual ~CapReader (){}

    virtual long read()=0;
};

#endif
