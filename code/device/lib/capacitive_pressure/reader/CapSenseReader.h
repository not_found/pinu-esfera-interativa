#ifndef CAPSENSE_READER_H
#define CAPSENSE_READER_H

#include "ArduinoFramework.h"
#include <CapacitiveSensor.h>

#include "CapReader.h"

class CapSenseReader : public CapReader{
public:
    CapSenseReader(
        int rcvPin,
        int sendPin,
        unsigned long timeoutMillis=30,
        uint8_t numSamples=30);


    long read() override;

protected:
    CapacitiveSensor c_sense;
    uint8_t numSamples;
};

#endif
