#ifndef PRESSURE_EVENT_DETECTOR_H
#define PRESSURE_EVENT_DETECTOR_H

#include "pressure/Event.h"
#include "pressure/EventListener.h"

#include "BoardConfig.h"

#include "DebounceFilter.h"
#include "LevelTrigger.h"

namespace pressure{

class EventDetector {
public:
    EventDetector (
        SensorId id = 0,
        float releaseThreshold=PRESSURE_RELEASE_THRESHOLD,
        float pressThreshold=PRESSURE_PRESS_THRESHOLD,
        int debounceThreshold=PRESSURE_DEBOUNCE_THRESHOLD);

    void reset();
    bool update(float relative, long raw);
    void onPressureEvent(EventListener listener);

    EventType state();

    SensorId id() const      { return _id; }
    SensorId id(SensorId id) { return _id = id; }

public:
    int debounceThreshold(int threshold);
    float lowerLevelThreshold(float threshold);
    float upperLevelThreshold(float threshold);

protected:
    void changeState(bool newState, float relative, long raw);

protected:
    SensorId _id;
    LevelTrigger<float> levelTrigger;
    DebounceFilter debounce;
    EventType currentState;
    EventListener listener;
};

}//namespace

using PressureEventDetector = pressure::EventDetector;

#endif
