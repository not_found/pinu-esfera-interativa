#ifndef COLOR_DEF_H
#define COLOR_DEF_H

#include "ArduinoFramework.h"

class ColorDef {
public:
    //Format #rrggbb
    static bool parse(const String & str, ColorDef & out);
    static ColorDef parse(const String & str);

public:
    ColorDef (uint8_t r=0, uint8_t g=0, uint8_t b=0);

    bool operator==(const ColorDef & other) const;
public:
  uint8_t r=0;
  uint8_t g=0;
  uint8_t b=0;
};

#endif
