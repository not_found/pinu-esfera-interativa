#include "ColorDef.h"

#include "log.h"

#include <cstdlib>

//Format #rrggbb
bool ColorDef::parse(const String & str, ColorDef & out){
    if (str.length() < 7 || str[0] != '#'){
        return false;
    }

    unsigned long colorCode = strtol(str.begin() + 1, nullptr, 16);

    if ((colorCode == 0 && str != "#000000")){
        return false;
    }

    out.r = (colorCode & 0xff0000) >> 16;
    out.g = (colorCode & 0x00ff00) >> 8;
    out.b = (colorCode & 0x0000ff);

    return true;
}

ColorDef ColorDef::parse(const String & str){
    ColorDef out;
    parse(str, out);
    return out;
}

ColorDef::ColorDef (uint8_t r, uint8_t g, uint8_t b)
    : r(r), g(g), b(b)
{}

bool ColorDef::operator==(const ColorDef & other) const{
    return r == other.r
        && g == other.g
        && b == other.b;
}
