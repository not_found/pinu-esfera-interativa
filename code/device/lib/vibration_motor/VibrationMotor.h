#ifndef VIBRATION_MOTOR_H
#define VIBRATION_MOTOR_H

#include "VibrationServices.h"

#include "TimeCounter.h"

class VibrationMotor: public VibrationServices {
public:
  VibrationMotor (int motorPin);

  void begin() override;
  void update() override;

  void vibrate(int durationMillis) override;
  bool isVibrating() override;
  void stop() override;

protected:
  int motorPin;
  bool vibrationEnabled;
  TimeCounter vibrationCounter;
};

#endif
