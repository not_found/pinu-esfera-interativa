#ifndef GYROSCOPE_H
#define GYROSCOPE_H

#include "ArduinoFramework.h"

#include "MPU6050.h"

#include <functional>

#include "GyroscopeMeasure.h"
#include "3dmath.hpp"

using GyroscopeListener = std::function<void(const GyroscopeMeasure &)>;

class Gyroscope{
protected:
    static volatile bool mpuInterrupt;
    static void dmpDataReady();

public:
    Gyroscope(int sclPin, int sdaPin, int interruptPin);
    bool update();

    bool begin();
    bool ready() const;

    void calibrate(int rounds=14);
    void calibrate(const Vector3D<float> & gravity, int rounds=14);

    void listen(GyroscopeListener listener);
    void stopListening();

public: //read measure
    Quat            & getQuaternion(const GyroscopeMeasure & measure, Quat & out);
    Vector3D<float> & getEuler(const GyroscopeMeasure & measure,
                              Vector3D<float>        & out,
                              bool                     inDegrees=true);
    Vector3D<float> & getYawPitchRow(const GyroscopeMeasure & measure,
                              Vector3D<float>        & out,
                              bool                     inDegrees=true);

    /** real acceleration, adjusted to remove gravity */
    Vector3D<int16_t> & getRealAccel(const GyroscopeMeasure & measure,
                                     Vector3D<int16_t>      & out);

    Vector3D<int16_t> & getWorldAccel(const GyroscopeMeasure & measure,
                                      Vector3D<int16_t>      & out);

public:
    void radiansToDegree(Vector3D<float> & vec);

public:
    MPU6050 & getMPU();

protected:
    bool setupPins();
    bool initDMP();
    void setupDefaultOffsets();
    void enableGyro();

    void readMeasures();
    void notifyMeasure(const GyroscopeMeasure & measure);

protected:
    int sclPin;
    int sdaPin;
    int interruptPin;

    // MPU control/status vars
    bool dmpReady;  // set true if DMP init was successful
    uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
    uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
    uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
    uint16_t fifoCount;     // count of all bytes currently in FIFO

    GyroscopeMeasure measure;
    GyroscopeListener listener;

    MPU6050 mpu;
};



#endif
