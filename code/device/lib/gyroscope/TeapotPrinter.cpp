#include "TeapotPrinter.h"

TeapotPrinter::TeapotPrinter()
    : teapotPacketCounter(0)
{}

void TeapotPrinter::outputTeapot(const GyroscopeMeasure & measure){
    outputTeapot(measure, Serial);
}

void TeapotPrinter::outputTeapot(const GyroscopeMeasure & measure, Print & out){
    auto buffer = measure.buffer();

    out.write('$');
    out.write((uint8_t)0x02);

    out.write((uint8_t)buffer[0]);
    out.write((uint8_t)buffer[1]);
    out.write((uint8_t)buffer[4]);
    out.write((uint8_t)buffer[5]);
    out.write((uint8_t)buffer[8]);
    out.write((uint8_t)buffer[9]);
    out.write((uint8_t)buffer[12]);
    out.write((uint8_t)buffer[13]);
    out.write((uint8_t)0x0);

    ++teapotPacketCounter; // loops at 0xFF on purpose
    out.write(teapotPacketCounter);
    out.write('\r');
    out.write('\n');
}
