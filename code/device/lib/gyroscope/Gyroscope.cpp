#include "MPU6050_6Axis_MotionApps20.h"

#include "Gyroscope.h"

#include "I2Cdev.h"
#include "helper_3dmath.h"


// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif


#define MPU6050_GRAVITY_MAGNITUDE 16384

volatile bool Gyroscope::mpuInterrupt = false;

void Gyroscope::dmpDataReady() {
    mpuInterrupt = true;
}

Gyroscope::Gyroscope(int sclPin, int sdaPin, int interruptPin)
  : sclPin(sclPin)
  , sdaPin(sdaPin)
  , interruptPin(interruptPin)
  , dmpReady(false)
{}

MPU6050 & Gyroscope::getMPU(){
    return mpu;
}

void Gyroscope::listen(GyroscopeListener listener){
  this->listener = listener;
}
void Gyroscope::stopListening(){
  listen(GyroscopeListener());
}

bool Gyroscope::begin(){
  if(!setupPins() || !initDMP()){
    return false;
  }

  setupDefaultOffsets();
  // calibrate();
  enableGyro();

  return true;
}

bool Gyroscope::ready() const{
    return dmpReady;
}

bool Gyroscope::setupPins(){
  // join I2C bus (I2Cdev library doesn't do this automatically)
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin(sdaPin, sclPin);
      Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(400, true);
  #endif

  mpu.initialize();
  pinMode(interruptPin, INPUT);

  if (!mpu.testConnection()) {
    Serial.println(F("MPU6050 connection failed"));
    return false;
  }

  return true;
}

bool Gyroscope::initDMP(){
  // load and configure the DMP
  // Serial.println(F("Initializing DMP..."));
  devStatus = mpu.dmpInitialize();

  if (devStatus != 0) {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));

    return false;
  }

  // get expected DMP packet size for later comparison
  packetSize = mpu.dmpGetFIFOPacketSize();
  measure.size(packetSize);

  return true;
}

void Gyroscope::setupDefaultOffsets(){
  // supply your own gyro offsets here, scaled for min sensitivity
  mpu.setXGyroOffset(220);
  mpu.setYGyroOffset(76);
  mpu.setZGyroOffset(-85);
  mpu.setZAccelOffset(1788); // 1688 factory default for my test chip
}

void Gyroscope::enableGyro(){
  mpu.setDMPEnabled(true);

  // enable Arduino interrupt detection
  // Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
  // Serial.print(digitalPinToInterrupt(interruptPin));
  // Serial.println(F(")..."));
  attachInterrupt(digitalPinToInterrupt(interruptPin), dmpDataReady, RISING);
  mpuIntStatus = mpu.getIntStatus();

  dmpReady = true;
}

void Gyroscope::calibrate(int rounds){
  calibrate({0,0,1}, rounds); //default gravity vector
}

void Gyroscope::calibrate(const Vector3D<float> & gravity, int rounds){
  Vector3D<float> gNormal = gravity.getNormalized(MPU6050_GRAVITY_MAGNITUDE);
  VectorInt16 g{(int16_t)gNormal.x(), (int16_t)gNormal.y(), (int16_t)gNormal.z()};

  // Calibration Time: generate offsets and calibrate our MPU6050
  mpu.CalibrateAccel(rounds, g);
  mpu.CalibrateGyro(rounds, g);
  // mpu.PrintActiveOffsets();
}

bool Gyroscope::update(){    // if programming failed, don't try to do anything
    if (!dmpReady) return false;

    if (mpuInterrupt) {
      fifoCount = mpu.getFIFOCount();

      // reset interrupt flag and get INT_STATUS byte
      mpuInterrupt = false;
      mpuIntStatus = mpu.getIntStatus();
    }

    if (fifoCount < packetSize) { //does not have a packet yet
        return false;
    }

    //check overflow
    if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        fifoCount = 0;  // will be zero after reset no need to ask
        Serial.println(F("FIFO overflow!"));

        return false;

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) {
        readMeasures();

        return true;
    }

    return false;
}

void Gyroscope::readMeasures(){
    // read all packets from FIFO
    while(fifoCount >= packetSize){ // Lets catch up to NOW, someone is using the dreaded delay()!
        mpu.getFIFOBytes(measure.buffer(), packetSize);
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;

        notifyMeasure(measure);
    }
}

void Gyroscope::notifyMeasure(const GyroscopeMeasure & measure){
  if (listener) {
    listener(measure);
  }
}

Quat & Gyroscope::getQuaternion(const GyroscopeMeasure & measure, Quat & out){
  Quaternion q;
  mpu.dmpGetQuaternion(&q, measure.buffer());

  out.set(q.x, q.y, q.z, q.w);

  return out;
}

Vector3D<float> &
Gyroscope::getEuler(const GyroscopeMeasure & measure, Vector3D<float> & out, bool inDegrees){
  Quaternion q;
  mpu.dmpGetQuaternion(&q, measure.buffer());
  mpu.dmpGetEuler(out.values, &q);

  if (inDegrees) {
    radiansToDegree(out);
  }

  return out;
}

Vector3D<float> & Gyroscope::getYawPitchRow(const GyroscopeMeasure & measure, Vector3D<float> & out, bool inDegrees){
  Quaternion q;
  VectorFloat gravity;

  mpu.dmpGetQuaternion(&q, measure.buffer());
  mpu.dmpGetGravity(&gravity, &q);
  mpu.dmpGetYawPitchRoll(out.values, &q, &gravity);

  if (inDegrees) {
    radiansToDegree(out);
  }

  return out;
}

Vector3D<int16_t> &
Gyroscope::getRealAccel(const GyroscopeMeasure & measure, Vector3D<int16_t> & out){
  Quaternion q;
  VectorFloat gravity;
  VectorInt16 aa;
  VectorInt16 aaReal;

  mpu.dmpGetQuaternion(&q, measure.buffer());
  mpu.dmpGetAccel(&aa, measure.buffer());
  mpu.dmpGetGravity(&gravity, &q);
  mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);

  out.set(aaReal.x, aaReal.y, aaReal.z);

  return out;
}

Vector3D<int16_t> &
Gyroscope::getWorldAccel(const GyroscopeMeasure & measure, Vector3D<int16_t> & out){
  Quaternion q;
  VectorFloat gravity;
  VectorInt16 aa, aaReal, aaWorld;

  mpu.dmpGetQuaternion(&q, measure.buffer());
  mpu.dmpGetAccel(&aa, measure.buffer());
  mpu.dmpGetGravity(&gravity, &q);
  mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);
  mpu.dmpGetLinearAccelInWorld(&aaWorld, &aaReal, &q);

  out.set(aaWorld.x, aaWorld.y, aaWorld.z);

  return out;
}


void Gyroscope::radiansToDegree(Vector3D<float> & vec){
  for (size_t i = 0; i < 3; i++) {
    vec.values[i] *= 180/M_PI;
  }
}
