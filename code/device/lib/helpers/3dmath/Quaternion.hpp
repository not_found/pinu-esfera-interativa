#ifndef MATH3D_QUATERNION_H
#define MATH3D_QUATERNION_H

#include "Vector3D.hpp"

template<typename T=float>
class QuaternionVec: public Vector3D<T, 4> {
public:
  using super = Vector3D<T, 4>;

public:
  QuaternionVec (T x=0, T y=0, T z=0, T w=1)
    : super(x,y,z)
  {
    this->values[3] = w;
  }

  QuaternionVec(T values[4])
  {
      Vector<T>::set(values, 4);
  }

public:
    using super::set;
  void set(T x, T y, T z, T w){
    super::set(x, y, z);

    this->w() = w;
  }

public:
  T & w(){ return this->values[3];}
};

using Quat = QuaternionVec<>;

#endif
