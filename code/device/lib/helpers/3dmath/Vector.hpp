#ifndef MATH3D_VECTOR_H
#define MATH3D_VECTOR_H

#include <cstddef>
#include "ArduinoFramework.h"

template<typename T>
class Vector {
public:
  virtual ~Vector (){}

  virtual std::size_t size() const = 0;
  virtual T * data() = 0;
  virtual const T * data() const = 0;

  virtual Vector<T> & operator=(const Vector<T> & other) {
      return set(other);
  }

  virtual T get(std::size_t i) const{
      if (i < size()){
          return data()[i];
      }

      return T(); //throw?
  }

  virtual void set(std::size_t i, const T & value){
      if (i < size()){
          this->data()[i] = value;
      }
      //throw?
  }

  template<typename U>
  Vector<T> & set(const Vector<U> & other){
      return this->set(other.data(), other.size());
  }
  template<typename U>
  Vector<T> & set(const U * data, std::size_t size){
      for (int i = 0; i < this->size() && i < size; ++i){
          this->data()[i] = data[i];
      }

      return *this;
  }

  virtual void normalize(float scale=1.0f){
      float m = getMagnitude();
      for (size_t i = 0; i < size(); i++) {
        data()[i] *= (scale/m);
      }
  }

  virtual Vector<T> & getNormalized(Vector<T> & out, float scale = 1.0f) const{
    out.set(*this);
    out.normalize(scale);

    return out;
  }

  virtual float getMagnitude() const {
    float sum=0;
    for (size_t i = 0; i < size(); i++) {
      sum += data()[i]* data()[i];
    }
    return sqrt(sum);
  }

  virtual String toString() const{
    String out{"["};

    for (size_t i = 0; i < size(); i++) {
      out.concat(data()[i]);

      if (i + 1 < size()) {
        out.concat(", ");
      }
    }

    out.concat("]");

    return out;
  }
};

#endif
