#ifndef MATH3D_VECTOR3D_H
#define MATH3D_VECTOR3D_H

#include "StaticVector.hpp"

#include <cstddef>

template<typename T, size_t N=3>
class Vector3D: public StaticVector<T, N> {
public:
  using super = StaticVector<T, N>;

public:
  using super::super; //super constructor
public:
  Vector3D (T x=0, T y=0, T z=0){
    set(x, y, z);
  }

public:
  void set(T x, T y, T z){
    this->x() = x;
    this->y() = y;
    this->z() = z;
  }

public:
  T & x(){ return this->values[0];}
  T & y(){ return this->values[1];}
  T & z(){ return this->values[2];}
};

#endif
