#ifndef CHRONOMETER_H
#define CHRONOMETER_H

class Chronometer{
public:
    Chronometer();
    virtual ~Chronometer(){}

    virtual void reset();
    virtual bool hasPassedMillis(int time);

protected:
    int lastUpdate;
};

#endif
