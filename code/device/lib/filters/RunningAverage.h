#ifndef RUNNING_AVERAGE
#define RUNNING_AVERAGE

#include <vector>
#include <cstddef>

#include "Filter.h"

class RunningAverage: public Filter<float>{
    int measuresLenght;
    float currentSum;
    int counter;
    int index;
    std::vector<float> measures;
public:
    RunningAverage(int lenght=10)
        : measuresLenght(lenght)
        , currentSum(0)
        , counter(0)
        , index(0)
    {
        measures.resize(measuresLenght, 0);
    }

    void reset(){
        index = 0;
        counter = 0;
        currentSum = 0;

        measures.resize(measuresLenght, 0);
        for (size_t i = 0; i < measuresLenght; i++) {
            measures[i] = 0;
        }
    }

    bool update(float value) override;

    float current() override{
        if (count() == 0){
            return 0;
        }
        return currentSum / count();
    }

    float sum() const{
        return currentSum;
    }

    int count() const{
        return counter;
    }

protected:
    void updateAverage(float newValue, float removedValue);
};

#endif
