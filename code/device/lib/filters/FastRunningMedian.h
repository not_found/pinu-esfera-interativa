#ifndef FAST_RUNNING_MEDIAN_H
#define FAST_RUNNING_MEDIAN_H

// adapted from: https://forum.arduino.cc/index.php?topic=53081.msg1160999#msg1160999
//
// Released to the public domain
//
// Remarks:
// This is a lean but fast version.
// Initially, the buffer is filled with a "default_value". To get real median values
// you have to fill the object with N values, where N is the size of the sliding window.
// For example: for(int i=0; i < 32; i++) myMedian.addValue(readSensor());
//
// Constructor:
// FastRunningMedian<datatype_of_content, size_of_sliding_window, default_value>
// maximim size_of_sliding_window is 255
// Methods:
// addValue(val) adds a new value to the buffers (and kicks the oldest)
// getMedian() returns the current median value
//
//
// Usage:
// #include "FastRunningMedian.h"
// FastRunningMedian<unsigned int,32, 0> myMedian;
// ....
// myMedian.addValue(value); // adds a value
// m = myMedian.getMedian(); // retieves the median
//

#include <inttypes.h>
#include <vector>

#include "RunningMedian.h"

template <typename T>
class FastRunningMedian : public RunningMedian<T>{
public:
	FastRunningMedian(uint8_t windowSize, T default_value=0)
	 	: counter(0)
		, buffer_idx(windowSize)
		, _window_size(windowSize)
		, _median_ptr(0)
	{
		// Init buffers
		// for(uint8_t i = 0; i < _window_size; ++i ) {
		// 	_inbuffer[i] = default_value;
		// 	_sortbuffer[i] = default_value;
		// }
		_inbuffer.resize(windowSize, default_value);
		_sortbuffer.resize(windowSize, default_value);
	};

	T getMedian() override{
		if (counter % 2 == 0  //even
		&& _median_ptr > 0)
		{
			return (_sortbuffer[_median_ptr]
					+ _sortbuffer[_median_ptr-1])/2;
		}

		return _sortbuffer[_median_ptr];
	}

	int windowSize() override{
		return _window_size;
	}

	int getCounter() override{
		return counter;
	}

	void addValue(T new_value) override{
		buffer_idx++;
		if (buffer_idx >= _window_size){
			buffer_idx = 0;
		}

		T old_value = _inbuffer[buffer_idx]; // retrieve the old value to be replaced
		_inbuffer[buffer_idx] = new_value;  // fill the new value in the cyclic buffer

		int insertion_idx = counter;

		if (counter == _window_size){
			if (new_value == old_value) 		  // if the value is unchanged, do nothing
				return;

			insertion_idx = search(old_value);
		}
		else {
			++counter;
			_median_ptr = counter/2;
		}

		_sortbuffer[insertion_idx] = new_value;

		insertion_sort(insertion_idx);
	}

	void insertion_sort(int i){
		// bubble up the value if needed
		for (int idx = i, next = i + 1;
			(next < counter) && _sortbuffer[idx] > _sortbuffer[next];)
		{
			swap(idx, next);
			++idx;
			++next;
		}

		for(int idx = i, prev = idx-1;
			(prev >= 0) && (_sortbuffer[idx] < _sortbuffer[prev]);)
		{
			swap(idx, prev);
			--idx;
			--prev;
		}
	}

	void swap(int p, int q){
		T tmp = _sortbuffer[p];
		_sortbuffer[p] = _sortbuffer[q];
		_sortbuffer[q] = tmp;
	}

	void __addValue(T new_value){
		// comparision with 0 is fast, so we decrement buffer_idx
		if (buffer_idx == 0)
			buffer_idx = _window_size;

		buffer_idx--;

		T old_value = _inbuffer[buffer_idx]; // retrieve the old value to be replaced
		if (new_value == old_value) 		  // if the value is unchanged, do nothing
			return;

		_inbuffer[buffer_idx] = new_value;  // fill the new value in the cyclic buffer

		// search the old_value in the sorted buffer
		uint8_t i = search(old_value);

		// i is the index of the old_value in the sorted buffer
		_sortbuffer[i] = new_value; // replace the value

		// the sortbuffer is always sorted, except the [i]-element..
		if (new_value > old_value) {
			//  if the new value is bigger than the old one, make a bubble sort upwards
			for(uint8_t p=i, q=i+1; q < _window_size; p++, q++) {
				// bubble sort step
				if (_sortbuffer[p] > _sortbuffer[q]) {
					T tmp = _sortbuffer[p];
					_sortbuffer[p] = _sortbuffer[q];
					_sortbuffer[q] = tmp;
				} else {
					// done ! - found the right place
					return;
				}
			}
		} else {
			// else new_value is smaller than the old one, bubble downwards
			for(int p=i-1, q=i; q > 0; p--, q--) {
				if (_sortbuffer[p] > _sortbuffer[q]) {
					T tmp = _sortbuffer[p];
					_sortbuffer[p] = _sortbuffer[q];
					_sortbuffer[q] = tmp;
				} else {
					// done !
					return;
				}
			}
		}
	}

protected:
	uint8_t search(T value){//To-Do: use binary search
		for(uint8_t i = 0; i < counter; ++i) {
			if (value == _sortbuffer[i])
				return i;
		}

		return -1;
	}

public:
	//count the number of values in buffer
	uint8_t counter;
	// Pointer to the last added element in _inbuffer
	uint8_t buffer_idx;
	// sliding window size
	uint8_t _window_size;
	// position of the median value in _sortbuffer
	uint8_t _median_ptr;

	// cyclic buffer for incoming values
	// T _inbuffer[N];
	std::vector<T> _inbuffer;
	// sorted buffer
	// T _sortbuffer[N];
	std::vector<T> _sortbuffer;
};

#endif
