#include "RunningAverage.h"

bool RunningAverage::update(float value){
    index = (index + 1) % measuresLenght;

    this->updateAverage(value, measures[index]);

    measures[index] = value;

    if (counter < measuresLenght){
        ++counter;
    }

    return true;
}

void RunningAverage::updateAverage(float newValue, float removedValue){
    currentSum += newValue - removedValue;
}
