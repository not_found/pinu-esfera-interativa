#ifndef RUNNING_MEDIAN_H
#define RUNNING_MEDIAN_H

#include "Filter.h"

template <typename T>
class RunningMedian: public Filter<T> {
public:
    virtual ~RunningMedian(){}

	virtual T getMedian() = 0;
    virtual int getCounter()=0;
    virtual int windowSize()=0;

    virtual void addValue(T new_value)=0;

    T current() override{
        return getMedian();
    }
    bool update(T value) override{
        this->addValue(value);
        return getCounter() == windowSize();
    }
};

#endif
