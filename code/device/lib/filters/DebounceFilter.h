#ifndef DEBOUNCE_FILTER_H
#define DEBOUNCE_FILTER_H

class DebounceFilter {
public:
    DebounceFilter(int countThreshold=5)
        : _state(false)
        , _countThreshold(countThreshold)
        , _countToChange(0)
    {}

    void reset(){
        resetCounter();
    }

    bool update(bool value){
        if (state() != value){
            if (++_countToChange == _countThreshold){
                this->_state = value;
                resetCounter();
                return true;
            }
        }
        else{
            resetCounter();
        }

        return false;
    }

    int countThreshold(int threshold){
        return _countThreshold = threshold;
    }
    int countThreshold(){
        return _countThreshold;
    }

    bool state() const{
        return _state;
    }

    bool state(bool newState){
        return _state = newState;
    }

protected:
    void resetCounter(){
        _countToChange = 0;
    }

protected:
    bool _state;
    int _countThreshold;
    int _countToChange;
};

#endif
