#ifndef FILTER_H
#define FILTER_H

template<typename T>
class Filter {
public:
    virtual ~Filter (){}

    virtual bool update(T value)=0;
    virtual T current()=0;
};

#endif
