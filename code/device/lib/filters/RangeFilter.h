#ifndef RANGE_FILTER_H
#define RANGE_FILTER_H

#include "Filter.h"

#include <limits>

template<typename T>
class RangeFilter: public Filter<T> {
public:
    RangeFilter (
        T default_value=0,
        T minValue=std::numeric_limits<T>::max(),
        T maxValue=std::numeric_limits<T>::lowest())
        : _minValue(minValue)
        , _maxValue(maxValue)
        , _current(default_value)
    {}

    bool update(T value){
        this->_current = value;

        if (value < _minValue){
            this->_minValue = value;
        }

        if (value > _maxValue){
            this->_maxValue = value;
        }

        return true;
    }

    T current() override{
        return _current;
    }

    float relative(){
        if (range() == 0){
            return 1.0f;
        }

        return diff()/(float)range();
    }

    T diff(){
        return current()-min();
    }

    T range(){
        return _maxValue - _minValue;
    }

    T min(){ return _minValue;}
    T max(){ return _maxValue;}

protected:
    T _minValue, _maxValue, _current;
};

#endif
