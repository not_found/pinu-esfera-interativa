#include "GyroscopeServices.h"

#include "BoardPinout.h"
#include "BoardConfig.h"

using namespace std::placeholders;

GyroscopeServices::GyroscopeServices()
    : GyroscopeServices(GYRO_SCL_PIN, GYRO_SDA_PIN, GYRO_INTERRUPT_PIN)
{}

GyroscopeServices::GyroscopeServices (int sclPin, int sdaPin, int interruptPin)
    : _gyro(sclPin, sdaPin, interruptPin)
    , currentFormat(OrientationFormat::Quaternion)
    , listener({})
    , notifyInterval(0)
{}

Gyroscope & GyroscopeServices::gyro(){
    return _gyro;
}

bool GyroscopeServices::ready() const{
    return _gyro.ready();
}
void GyroscopeServices::begin(){
    _gyro.begin();
}
void GyroscopeServices::update(){
    _gyro.update();
}

void GyroscopeServices::calibrate(){
    this->calibrate({DEFAULT_GRAVITY_X, DEFAULT_GRAVITY_Y, DEFAULT_GRAVITY_Z});
}

void GyroscopeServices::calibrate(const Vector3D<float> & gravity){
    this->_gyro.calibrate(gravity);
}


void GyroscopeServices::
    listenOrientation(OrientationListener listener, ListenOrientationConfig config)
{
    this->listener = listener;
    this->currentFormat = config.format;
    this->notifyInterval.setTime(max(config.timeInterval, GYROSCOPE_MINIMAL_INTERVAL));

    this->_gyro.listen(std::bind(&GyroscopeServices::onMeasure, this, _1));
}

void GyroscopeServices::removeListener(){
    this->listener = {};
    this->_gyro.listen({});
}

void GyroscopeServices::onMeasure(const GyroscopeMeasure & m){
    if (!listener || !notifyInterval.completed()){
        return;
    }

    switch (currentFormat){
        case OrientationFormat::Quaternion:{
            Quat q;
            listener(_gyro.getQuaternion(m, q));
            break;
        }
        case OrientationFormat::Euler:{
            Vector3D<float> vec3;
            listener(_gyro.getEuler(m, vec3));
            break;
        }
        case OrientationFormat::YawPitchRow:{
            Vector3D<float> vec3;
            listener(_gyro.getYawPitchRow(m, vec3));
            break;
        }
    }

    notifyInterval.reset();
}
