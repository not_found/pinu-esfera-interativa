#ifndef GYRO_ORIENTATION_SERVICES_H
#define GYRO_ORIENTATION_SERVICES_H

#include "OrientationServices.h"
#include "Gyroscope.h"
#include "TimeCounter.h"

class GyroscopeServices: public OrientationServices {
public:
    GyroscopeServices ();
    GyroscopeServices (int sclPin, int sdaPin, int interruptPin);

    bool ready() const;
    void begin() override;
    void update() override;

    void calibrate() override;
    void calibrate(const Vector3D<float> & gravity) override;

    void listenOrientation(OrientationListener listener, ListenOrientationConfig config) override;
    void removeListener() override;
public:
    Gyroscope & gyro();

protected:
    void onMeasure(const GyroscopeMeasure & m);

protected:
    Gyroscope _gyro;
    OrientationFormat currentFormat;
    OrientationListener listener;
    TimeCounter notifyInterval;
};
#endif
