#include "tests.h"

#include "BuzzerFixture.h"

void test_buzzer_simple(){
  BuzzerFixture().test_buzzer_simple();
}

void test_buzzer_melody(){
  BuzzerFixture().test_buzzer_melody();
}
