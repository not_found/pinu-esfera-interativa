#ifndef BUZZER_FIXTURE_H
#define BUZZER_FIXTURE_H

#include <unity.h>
#include <ArduinoFramework.h>

#include "BuzzerSoundServices.h"

#include "BoardPinout.h"

class BuzzerFixture{
  BuzzerSoundServices buzzer;

public:
  BuzzerFixture()
    : buzzer(BUZZER_PIN, BUZZER_CHANNEL)
  {
    buzzer.begin();
  }

  ~BuzzerFixture(){
    buzzer.stop();
  }

  void test_buzzer_simple(){
    TEST_ASSERT_FALSE_MESSAGE(buzzer.isPlaying(), "When created, should not be playing.");

    buzzer.play({{2000, 300, 300}});
    TEST_ASSERT_TRUE_MESSAGE(buzzer.isPlaying(), "After play, should be playing");
    TEST_ASSERT_FALSE_MESSAGE(buzzer.isOnPause(), "After play, should no be on pause");

    delay(310);
    TEST_ASSERT_TRUE(buzzer.isPlaying());
    TEST_ASSERT_FALSE(buzzer.isOnPause());
    buzzer.update();
    TEST_ASSERT_TRUE_MESSAGE(buzzer.isPlaying(), "When tone duration is over, should still be playing");
    TEST_ASSERT_TRUE_MESSAGE(buzzer.isOnPause(), "When tone duration is over, should start pause");

    delay(310);
    TEST_ASSERT_TRUE(buzzer.isPlaying());
    TEST_ASSERT_TRUE(buzzer.isOnPause());
    buzzer.update();
    TEST_ASSERT_FALSE_MESSAGE(buzzer.isPlaying(), "When play time is over, should not be playing anymore");
    TEST_ASSERT_FALSE_MESSAGE(buzzer.isOnPause(), "When play time is over, should not be on pause");
  }

  void test_buzzer_melody(){
    auto melody = SoundServices::Melody{
      {2000, 300, 300},
      {3000, 300, 300},
      {3500, 300, 300},
    };

    buzzer.play(melody);

    TEST_ASSERT_EQUAL_MESSAGE(0, buzzer.currentToneIndex(), "Should start playing at first index");

    waitTone(melody[0]);
    TEST_ASSERT_EQUAL_MESSAGE(1, buzzer.currentToneIndex(), "After tone finish should play next");

    for (size_t i = 1; i < melody.size(); i++) {
      waitTone(melody[i]);
    }

    TEST_ASSERT_FALSE_MESSAGE(buzzer.isPlaying(), "After playing all notes, should stop playing");
    TEST_ASSERT_EQUAL(3, buzzer.currentToneIndex());
  }

  void waitTone(const ToneDef & tone, int extraTime=5){
    delay(tone.duration + extraTime);
    buzzer.update();

    delay(tone.pause + extraTime);
    buzzer.update();

  }

  void updateUntil(int time){
    TimeCounter counter;
    counter.resetTimer(time);

    while(!counter.completed()){
      buzzer.update();

      delay(10);
    }
  }

};

#endif
