#ifndef SERVICES_TESTS_H
#define SERVICES_TESTS_H

void test_services_config();
void test_ledrgb();
void test_vibration();
void test_buzzer_simple();
void test_buzzer_melody();

#endif
