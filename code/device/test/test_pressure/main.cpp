#include <unity.h>

#include "ArduinoFramework.h"

#include "PressureSensorTests.h"
#include "FiltersTest.h"

void setup(){
    UNITY_BEGIN();
    RUN_TEST(test_raw_reader);
    RUN_TEST(test_mux_reader);

    RUN_TEST(test_running_average);
    RUN_TEST(test_running_median_odd);
    RUN_TEST(test_running_median_even);
    RUN_TEST(test_running_median_error);

    RUN_TEST(test_pressure_sensor);
    UNITY_END();
}

void loop()
{}
