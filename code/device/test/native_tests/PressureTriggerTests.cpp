#include "PressureTriggerTests.h"

#include <fstream>
#include <iostream>
#include <functional>
#include <cstddef>

#include "PressureEventDetector.h"
#include "LevelTrigger.h"
#include "DebounceFilter.h"

#include "TimeCounter.h"
#include "log.h"

using namespace std::placeholders;
using std::cout;
using std::endl;

void run_test_level_trigger()
{ PressureTriggerTests().test_level_trigger();}

void run_test_debounce()
{ PressureTriggerTests().test_debounce();}

void run_test_detection()
{ PressureTriggerTests().test_detection();}

void run_test_simple_detection()
{ PressureTriggerTests().test_simple_detection();}

void PressureTriggerTests::runTests(){
    RUN_TEST(run_test_level_trigger);
    RUN_TEST(run_test_debounce);
    RUN_TEST(run_test_detection);
    RUN_TEST(run_test_simple_detection);
}

PressureTriggerTests::PressureTriggerTests()
    : count_press_events(0)
    , count_release_events(0)
{}


void PressureTriggerTests::test_level_trigger(){
    float lowerThreshold = PRESSURE_RELEASE_THRESHOLD;
    float upperThreshold = PRESSURE_PRESS_THRESHOLD;

    LevelTrigger<float> trigger{lowerThreshold, upperThreshold};

    TEST_ASSERT_FALSE_MESSAGE(trigger.on(),
        "LevelTrigger should start off");

    bool changed = trigger.update(upperThreshold - 0.1f);
    TEST_ASSERT_FALSE_MESSAGE(changed,
        "State should not change if value does not pass threshold");
    TEST_ASSERT_FALSE_MESSAGE(trigger.on(),
        "State should still be false");

    trigger.update(upperThreshold);
    TEST_ASSERT_TRUE_MESSAGE(trigger.on(),
        "When value is equal or greater upper threshold state should change to ON");

    TEST_ASSERT_FALSE_MESSAGE(trigger.update(lowerThreshold + 0.1f),
        "State should not change if value does not pass threshold");
    TEST_ASSERT_TRUE_MESSAGE(trigger.on(),
        "State should still be ON");

    trigger.update(lowerThreshold);
    TEST_ASSERT_TRUE_MESSAGE(trigger.off(),
        "When value is equal or less than lower threshold state should change to OFF");
}

void PressureTriggerTests::test_debounce(){
    debounce.reset();

    TEST_ASSERT_FALSE_MESSAGE(debounce.state(),
        "Debounce state should start OFF");

    checkDebounceChangeState(5);
    checkDebounceChangeState(5);
    // checkDebounceChangeState(0, 200);
    // checkDebounceChangeState(0, 200);
}

void PressureTriggerTests::
    checkDebounceChangeState(int countThreshold, int timeoutMillis)
{
    debounce.countThreshold(countThreshold);
    debounce.reset();

    bool initState = debounce.state();

    bool shouldFail=true;

    for (int count = 0; count < 2; ++count){
        for(int i=0; i < countThreshold - 1; ++i){
            TEST_ASSERT_FALSE_MESSAGE(debounce.update(!initState),
                "Debounce should not change before count threshold equal updates");
        }

        if (shouldFail){
            TEST_ASSERT_FALSE_MESSAGE(debounce.update(initState),
                "Should reset if updated with initial value before reach threshold");

            shouldFail = false;
        }
    }

    TEST_ASSERT_TRUE_MESSAGE(debounce.update(!initState),
        "Debounce update should notify when have changed");
    TEST_ASSERT_EQUAL_MESSAGE(!initState, debounce.state(),
        "Debounce state should have changed");
}

void PressureTriggerTests::test_simple_detection(){
    resetDetector();
    detector.debounceThreshold(5);
    detector.lowerLevelThreshold(0.4f);
    detector.upperLevelThreshold(0.6f);

    std::vector<SensorMeasure> pressReleasePattern{
        {20, 0.2f},  {30, 0.3f},  {10, 0.1f}, {50, 0.5f}, //low
        //almost press:
        {60, 0.6f}, {61, 0.61f}, {67, 0.67f}, {10, 1.0f}, {59, 0.59f},
        //trigger press:
        {60, 0.6f},  {70, 0.7f}, {62, 0.62f}, {80, 0.8f}, {61, 0.61f},
        //near release:
        {59, 0.59f}, {20, 0.2f}, {30, 0.3f}, {59, 0.59f}, {60, 0.60f},
        //release:
        {40, 0.40f}, {39, 0.39f}, {30, 0.3f}, {0, 0.0f}, {40, 0.4f},
    };

    checkDetector(pressReleasePattern, 1, 1);
}

void PressureTriggerTests::test_detection(){
    auto measures = readMeasures("test_data/pressure_sensor_measures.txt");

    resetDetector();

    checkDetector(measures.measures(), 2, 2);
}

void PressureTriggerTests::checkDetector(
    const std::vector<SensorMeasure> & measures,
    int expectedPresses,
    int expectedReleases)
{
    for (int i=0; i<measures.size(); ++i){
        const auto & m = measures[i];

        if(detector.update(m.second, m.first)){
            if (detector.state() == pressure::EventType::Press){
                cout << "Pressed";
            }
            else{
                cout << "Released";
            }
            cout << " at " << i
                 << " value (" << m.first << "," << m.second << ")"
                 << endl;
        }
    }

    TEST_ASSERT_EQUAL_MESSAGE(expectedPresses, count_press_events,
        "Detected press events does not match the expected");
    TEST_ASSERT_EQUAL_MESSAGE(expectedReleases, count_release_events,
        "Detected release events does not match the expected");
}

void PressureTriggerTests::resetDetector(){
    count_press_events = 0;
    count_release_events = 0;

    detector.reset();
    detector.onPressureEvent(std::bind(&PressureTriggerTests::onPressureEvent, this, _1));
}

void PressureTriggerTests::onPressureEvent(const pressure::Event & evt){
    if (evt.evtType == pressure::EventType::Press){
        ++count_press_events;
    }

    if (evt.evtType == pressure::EventType::Release){
        ++count_release_events;
    }
}

SensorMeasures PressureTriggerTests::readMeasures(const std::string & filepath){
    SensorMeasures measures;

    std::ifstream f;
    f.open(filepath);

    if (f.is_open()){
        long raw;
        float relative;

        while(f){
            f >> raw >> relative;

            measures.add(raw, relative);
            // cout << raw << ", " << relative << endl;
        }
    }
    else{
        std::cerr << "failed to open!" << std::endl;
    }



    return measures;
}
