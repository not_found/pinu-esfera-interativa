#include "ParseTests.h"

#include "unity.h"

#include "Request.h"

#include "ColorDef.h"

#include "log.h"


using namespace api;
using namespace pressure;

void test_parser__serialize_response(){ ParseTests().testSerializeResponse();}
void test_parser__serialize_pressure_sensor(){ ParseTests().testSerializePressureSensor();}
void test_parser__serialize_pressure_sensors(){ ParseTests().testSerializePressureSensors();}
void test_parser__serialize_routes(){ ParseTests().testSerializeRoutes();}
void test_parser__color_conversion()      { ParseTests().testColorConversion();}

void ParseTests::runTests(){
    RUN_TEST(test_parser__serialize_response);
    RUN_TEST(test_parser__serialize_pressure_sensor);
    // RUN_TEST(test_serialize_pressure_sensors);
    RUN_TEST(test_parser__serialize_routes);
    RUN_TEST(test_parser__color_conversion);
}

void ParseTests::testSerializeResponse(){
    checkParseResponse(Response{123},
                        "123 200 Ok\n\n");
    checkParseResponse(Response{123, Response::Status::BadRequest},
                        "123 400 Bad request\n\n");
    checkParseResponse(Response{456, Response::Status::ServerError, "Gyroscope not ready or disconnected"},
                        "456 500 Gyroscope not ready or disconnected\n\n");

    String msgData = "[12.4,15.6,17.8]";
    auto r = Response(78, Response::Status::Notification).data(msgData);
    checkParseResponse(r,
        "78 210 Notification\n" +
        String("len: ") + msgData.length() + "\n\n" +
        msgData);

    r.args({{"format", "example"}});
    checkParseResponse(r,
        String("78 210 Notification\n") +
        "format: example\n" +
        "len: " + msgData.length() + "\n\n" +
        msgData);
}

void ParseTests::testSerializePressureSensor(){
    SensorDescription sensor{1, "name", "sensor description"};
    String result = parser.serializePressureSensor(sensor);
    String expected = R"({"id":1,"name":"name","desc":"sensor description"})";

    TEST_ASSERT_EQUAL_STRING_MESSAGE(
        expected.c_str(),
        result.c_str(),
        "Serialized pressure sensor does not match the expected.");
}

void ParseTests::testSerializePressureSensors(){
    SensorDescriptions sensors{
        {1, {1, "name", "sensor description"}},
        {2, {2, "up"}},
        {3, {3, "front"}}
    };
    String result = parser.serializePressureSensors(sensors);
    String expected = "["
        R"({"id":1,"name":"name","desc":"sensor description"})" ","
        R"({"id":2,"name":"up"})" ","
        R"({"id":3,"name":"front"})"
        "]";


    //WARNING: this fail, because SensorDescriptions is an UNORDERED map
    TEST_ASSERT_EQUAL_STRING_MESSAGE(
        expected.c_str(),
        result.c_str(),
        "Serialized pressure sensors does not match the expected.");
}


void ParseTests::testSerializeRoutes(){
    auto routeStr = parser.serializeRoutes({
        {{"/"}, {}},
        {{"/example", Methods::Get}, {}},
        {{"/example", Methods::Post}, {}}
    });

    TEST_ASSERT_EQUAL_STRING_MESSAGE(
        R"({"/":["*"],"/example":["Get","Post"]})",
        routeStr.c_str(),
        "Serialized routes does not match the expected value");
}

void ParseTests::testColorConversion(){
    ColorDef c = ColorDef::parse("#ffaabb");

    TEST_ASSERT_EQUAL_MESSAGE(0xff, c.r, "Color 'red' channel does not match");
    TEST_ASSERT_EQUAL_MESSAGE(0xaa, c.g, "Color 'green' channel does not match");
    TEST_ASSERT_EQUAL_MESSAGE(0xbb, c.b, "Color 'blue' channel does not match");
}

void ParseTests::checkParseResponse(const Response & response, const String & expected){
    auto responseStr = parser.serializeResponse(response);

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected.c_str(), responseStr.c_str(),
        "Serialized response does not match the expected");
}
