#include <unity.h>

#include "ArduinoFramework.h"

#include "PressureTriggerTests.h"
#include "ParseTests.h"
#include "ApiRouterTests.h"
#include "ApiTests.h"
#include "ApiRequestReaderTests.h"

int main()
{
    UNITY_BEGIN();

    // PressureTriggerTests::runTests();
    ParseTests::runTests();
    ApiRouterTests::runTests();
    ApiTests::runTests();
    ApiRequestReaderTests::runTests();

    UNITY_END();
}
