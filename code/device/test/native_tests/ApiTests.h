#ifndef API_TESTS_H
#define API_TESTS_H

#include <ArduinoFake.h>

#include "SphereServices.h"
#include "SphereApi.h"
#include "ApiParser.h"
#include "Response.h"

#include "StringStream.h"

using fakeit::Mock;

using pressure::SensorDescription;

class ApiTests {
public:
    static void runTests();

public:
    ApiTests();

public:
    void testSetup();
    void testDisconnected();
    void test_led_services();
    void test_vibration_services();
    void test_sound_services();
    void test_orientation_api_calibrate();
    void testOrientationApi_listenOrientation();
    void test_pressure_services();

protected:
    void mockupServices();

    void givenSensors(std::vector<SensorDescription> sensors);

    void callApi(String header);
    void checkResponse(const api::Response & expected);
    void checkNotifyOrientation(
        int expectedMessageId,
        OrientationFormat orientationFormat);
    void checkNotifyPressureEvent(int expectedMessageId);

protected:
    Mock<LedServices> ledServices;
    Mock<VibrationServices> vibrationServices;
    Mock<SoundServices> soundServices;
    Mock<OrientationServices> orientationServices;
    Mock<PressureServices> pressureServices;

    SphereServices services;

    StringStream stream;
    api::SphereApi api;
    api::Parser parser;

    OrientationListener orientationListener;
    PressureListener    pressureListener;
    SensorDescriptions sensors;
};


#endif
