#include "ApiTests.h"

#include "unity.h"

#include "StringStream.h"
#include "log.h"
#include "3dmath/Quaternion.hpp"
#include "3dmath/Vector3D.hpp"

#include <exception>

using namespace fakeit;
using namespace api;

void tryTest(std::function<void()> test){
    try
    {
        test();
    }
    catch(const String & e){
        TEST_FAIL_MESSAGE(e.c_str());
    }
    catch(const std::exception & e){
        TEST_FAIL_MESSAGE(e.what());
    }
    catch (...){
        TEST_FAIL_MESSAGE("Failed due to unknown exception");
    }
}

#define TRY_TEST(call) tryTest([&](){call;})

void test_api_services_setup()      { ApiTests().testSetup();}
void test_api_disconnection()       { ApiTests().testDisconnected();}
void test_api_led_services()        { TRY_TEST(ApiTests().test_led_services());}
void test_api_vibration_services()  { TRY_TEST(ApiTests().test_vibration_services());}
void test_api_sound_services()      { TRY_TEST(ApiTests().test_sound_services());}
void test_api_orientation_calibrate(){ TRY_TEST(ApiTests().test_orientation_api_calibrate());}
void test_api_orientation_listen()  { TRY_TEST(ApiTests().testOrientationApi_listenOrientation());}
void test_api_pressure_services()   { TRY_TEST(ApiTests().test_pressure_services());}

void ApiTests::runTests(){
    RUN_TEST(test_api_services_setup);
    RUN_TEST(test_api_disconnection);
    RUN_TEST(test_api_led_services);
    RUN_TEST(test_api_vibration_services);
    RUN_TEST(test_api_sound_services);
    RUN_TEST(test_api_orientation_calibrate);
    RUN_TEST(test_api_orientation_listen);
    RUN_TEST(test_api_pressure_services);
}

ApiTests::ApiTests()
    : ledServices()
    , vibrationServices()
    , soundServices()
    , orientationServices()
    , pressureServices()
    , services({
        &ledServices.get(),
        &vibrationServices.get(),
        &soundServices.get(),
        &orientationServices.get(),
        &pressureServices.get()
     })
     , stream()
     , api(&stream, services)
{
    api.setup();

    mockupServices();
}

void ApiTests::mockupServices(){
    Fake(Method(ledServices, changeColor));
    Fake(Method(ledServices, off));

    Fake(Method(orientationServices, removeListener));
    Fake(OverloadedMethod(orientationServices, calibrate, void()));
    Fake(OverloadedMethod(orientationServices, calibrate, void(const Vector3D<float> &)));
    When(Method(orientationServices, listenOrientation))
        .AlwaysDo([this](OrientationListener listener, ListenOrientationConfig config){
            this->orientationListener = listener;
        });

    Fake(Method(pressureServices, removeListener));
    When(Method(pressureServices, listenPressureEvents))
        .AlwaysDo([this](PressureListener listener){
            this->pressureListener = listener;
        });

}

void ApiTests::givenSensors(std::vector<SensorDescription> sensors){
    this->sensors.clear();
    for (auto & s : sensors){
        sensors[s.id()] = std::move(s);
    }

    When(Method(pressureServices, getSensorDescriptions))
        .AlwaysReturn(this->sensors);
}

void ApiTests::testSetup(){
    TEST_ASSERT_NOT_NULL(services.led());
    TEST_ASSERT_NOT_NULL(services.vibration());
    TEST_ASSERT_NOT_NULL(services.sound());
    TEST_ASSERT_NOT_NULL(services.orientation());
    TEST_ASSERT_NOT_NULL(services.pressure());
}

void ApiTests::testDisconnected(){
    this->api.disconnected();

    Verify(Method(orientationServices, removeListener));
    Verify(Method(pressureServices, removeListener));
}

void ApiTests::test_led_services(){
    callApi("PUT /led/color 123\ncolor: #66ff66\n\n");
    Verify(Method(ledServices, changeColor).Using(ColorDef(0x66,0xff,0x66))).Once();
    checkResponse(Response{123, Response::Status::Ok});

    callApi("POST /led/off 1\n\n");
    Verify(Method(ledServices, off)).Once();
    checkResponse(Response{1, Response::Status::Ok});
}

void ApiTests::test_vibration_services(){
}
void ApiTests::test_sound_services(){

}
void ApiTests::test_orientation_api_calibrate(){
    callApi("POST /orientation/calibrate 654\n\n");
    Verify(OverloadedMethod(orientationServices, calibrate, void())).Once();
    checkResponse(Response{654, Response::Status::Ok});

    callApi("POST /orientation/calibrate 654\ngravity: [0, 0, 1]\n\n");
    Verify(OverloadedMethod(orientationServices, calibrate, void(const Vector3D<float> &))).Once();
    checkResponse(Response{654, Response::Status::Ok});

    callApi("POST /orientation/calibrate 654\ngravity: [0, \n\n");
    checkResponse(Response{654, Response::Status::BadFormat});
}

void ApiTests::testOrientationApi_listenOrientation(){
    callApi("LISTEN /orientation 987\n\n");
    Verify(Method(orientationServices, listenOrientation)
            .Using(_, ListenOrientationConfig{OrientationFormat::Quaternion})).Once();
    checkResponse(Response{987, Response::Status::Ok});

    callApi("LISTEN /orientation 988\nformat: euler\n\n");
    Verify(Method(orientationServices, listenOrientation)
            .Using(_, ListenOrientationConfig{OrientationFormat::Euler})).Once();
    checkResponse(Response{988, Response::Status::Ok});

    callApi("LISTEN /orientation 989\nformat: YawPitchRow\n\n");
    Verify(Method(orientationServices, listenOrientation)
            .Using(_, ListenOrientationConfig{OrientationFormat::YawPitchRow})).Once();
    checkResponse(Response{989, Response::Status::Ok});

    checkNotifyOrientation(989, OrientationFormat::YawPitchRow);

    
    callApi("LISTEN /orientation 989\ninterval: 15\n\n");
    Verify(Method(orientationServices, listenOrientation)
            .Using(_, ListenOrientationConfig{OrientationFormat::Quaternion, 15})).Once();
    checkResponse(Response{989, Response::Status::Ok});


    callApi("UNLISTEN /orientation 989\n\n");
    Verify(Method(orientationServices, removeListener)).Once();
    checkResponse(Response{989, Response::Status::Ok});
}

void ApiTests::test_pressure_services(){
    givenSensors({
        {0, "front", "Front button"},
        {1, "top"  , "Top button"}
    });

    callApi("GET /pressure/sensors 3456\n\n");
    Verify(Method(pressureServices, getSensorDescriptions)).Once();
    checkResponse(Response{3456, Response::Status::Ok}
                    .data(parser.serializePressureSensors(sensors)));

    callApi("LISTEN /pressure/sensors 3457\n\n");
    Verify(Method(pressureServices, listenPressureEvents).Using(_)).Once();
    checkResponse(Response{3457, Response::Status::Ok});

    checkNotifyPressureEvent(3457);

    callApi("UNLISTEN /pressure/sensors 3457\n\n");
    Verify(Method(pressureServices, removeListener)).Once();
    checkResponse(Response{3457, Response::Status::Ok});
}

void ApiTests::callApi(String header){
    stream.inStr(std::move(header));
    this->api.update();
}

void ApiTests::checkResponse(const api::Response & expected){
    auto responseStr = std::move(stream.outStr());
    auto expectedStr = parser.serializeResponse(expected);

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expectedStr.c_str(), responseStr.c_str(),
        "Response does not match the expected");
}

void ApiTests::checkNotifyOrientation(int expectedMessageId, OrientationFormat orientationFormat)
{
    TEST_ASSERT_TRUE_MESSAGE((bool)orientationListener,
        "Should have captured a valid orientation listener");

    Quat quaternion{1, 2.2f, 3.33f, 4.444f};
    Vector3D<float> vector{-1, 0, 1.5f};
    String expectedData;

    switch (orientationFormat){
    case OrientationFormat::Euler:
    case OrientationFormat::YawPitchRow:
        orientationListener(vector);
        expectedData = vector.toString();
        break;
    case OrientationFormat::Quaternion:
    default:
        orientationListener(quaternion);
        expectedData = quaternion.toString();
        break;
    }

    checkResponse({
        expectedMessageId, Response::Status::Notification, "",
        {{"format", orientationFormat.c_str()}},
        expectedData
    });
}

void ApiTests::checkNotifyPressureEvent(int expectedMessageId){
    TEST_ASSERT_TRUE_MESSAGE((bool)pressureListener,
        "Should have captured a valid pressure listener");

    pressure::Event evt{1, pressure::EventType::Press, 0.50f, 3432};
    String expectedData = R"({"sensor":1,"type":"Press","relative":0.5,"raw":3432})";

    pressureListener(evt);

    checkResponse(
        Response{expectedMessageId, Response::Status::Notification}
            .data(expectedData)
    );
}
