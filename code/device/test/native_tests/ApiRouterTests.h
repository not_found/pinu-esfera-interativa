#ifndef API_ROUTER_TESTS_H
#define API_ROUTER_TESTS_H

#include "ApiRouter.h"

class ApiRouterTests {
public:
    static void runTests();

public:
    ApiRouterTests()
        : routeCalled(false)
    {}

public:
    void test_router();
    void test_default_route();
    void testMethodRoutes();

protected:
    void configureSomeRoutes();

    void addRoute(const String & path, api::Methods m = api::Methods::Any);
    api::Route makeRoute(const String & path, api::Methods m = api::Methods::Any);

    void routeRequest(api::Request req);
    void checkRouteCalled(const api::RoutePath & expected);

protected:
    bool routeCalled;
    api::RoutePath routePathCalled;
    api::Router router;
};
#endif
