#ifndef PRESSURE_TRIGGER_TESTS_H
#define PRESSURE_TRIGGER_TESTS_H

#include "unity.h"

#include <vector>
#include <string>
#include <cstddef>
#include <utility>

#include "PressureEventDetector.h"
#include "DebounceFilter.h"

using SensorMeasure = std::pair<long, float>;

class SensorMeasures {
public:
    void add(long rawValue, float relativeValue){
        values.push_back({rawValue, relativeValue});
    }

    std::size_t size() const{
        return values.size();
    }

    long raw(std::size_t i){
        return values[i].first;
    }

    float relative(std::size_t i){
        return values[i].second;
    }

    const std::vector<SensorMeasure> & measures(){
        return values;
    }

protected:
    std::vector<SensorMeasure> values;
};

class PressureTriggerTests {
public:
    static void runTests();

public:
    PressureTriggerTests();

public:
    void test_level_trigger();
    void test_debounce();
    void test_simple_detection();
    void test_detection();

protected:
    void checkDebounceChangeState(int countThreshold, int timeoutMillis=0);

    void checkDetector(const std::vector<SensorMeasure> & measures,
        int expectedPresses,
        int expectedReleases);

protected:
    SensorMeasures readMeasures(const std::string & file);

    void resetDetector();
    void onPressureEvent(const pressure::Event & evt);

protected:
    DebounceFilter debounce;
    pressure::EventDetector detector;
    int count_press_events, count_release_events;
};

#endif
