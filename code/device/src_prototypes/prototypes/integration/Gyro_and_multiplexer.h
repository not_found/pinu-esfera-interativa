

// uncomment "OUTPUT_READABLE_QUATERNION" if you want to see the actual
// quaternion components in a [w, x, y, z] format (not best for parsing
// on a remote host such as Processing or something though)
//#define OUTPUT_READABLE_QUATERNION

// uncomment "OUTPUT_READABLE_EULER" if you want to see Euler angles
// (in degrees) calculated from the quaternions coming from the FIFO.
// Note that Euler angles suffer from gimbal lock (for more info, see
// http://en.wikipedia.org/wiki/Gimbal_lock)
// #define OUTPUT_READABLE_EULER

// uncomment "OUTPUT_READABLE_YAWPITCHROLL" if you want to see the yaw/
// pitch/roll angles (in degrees) calculated from the quaternions coming
// from the FIFO. Note this also requires gravity vector calculations.
// Also note that yaw/pitch/roll angles suffer from gimbal lock (for
// more info, see: http://en.wikipedia.org/wiki/Gimbal_lock)
// #define OUTPUT_READABLE_YAWPITCHROLL

// uncomment "OUTPUT_READABLE_REALACCEL" if you want to see acceleration
// components with gravity removed. This acceleration reference frame is
// not compensated for orientation, so +X is always +X according to the
// sensor, just without the effects of gravity. If you want acceleration
// compensated for orientation, us OUTPUT_READABLE_WORLDACCEL instead.
//#define OUTPUT_READABLE_REALACCEL

// uncomment "OUTPUT_READABLE_WORLDACCEL" if you want to see acceleration
// components with gravity removed and adjusted for the world frame of
// reference (yaw is relative to initial orientation, since no magnetometer
// is present in this case). Could be quite handy in some cases.
//#define OUTPUT_READABLE_WORLDACCEL

// uncomment "OUTPUT_TEAPOT" if you want output that matches the
// format used for the InvenSense teapot demo
#define OUTPUT_TEAPOT

#include "classes/GyroPrototype.h"
#include "classes/MultiplexerWriteAll.h"

#define S0_PIN 5
#define S1_PIN 17
#define S2_PIN 16
#define S3_PIN 4
#define SIG_PIN 2


#define INTERRUPT_PIN 19  // use pin 2 on Arduino Uno & most boards
#define SCL_PIN 22
#define SDA_PIN 21

GyroPrototype gyro;
MultiplexerWriteAll multiplexer(S0_PIN, S1_PIN, S2_PIN, S3_PIN);

int lastUpdate=0;

void setup(){
    delay(1);
    Serial.begin(115200);

    gyro.setup(SCL_PIN, SDA_PIN, INTERRUPT_PIN);
    multiplexer.setup(SIG_PIN);

    lastUpdate = millis();
}

void loop(){
    gyro.update();

    if(millis() - lastUpdate > 2000){
        lastUpdate = millis();
        multiplexer.update();
    }
}
