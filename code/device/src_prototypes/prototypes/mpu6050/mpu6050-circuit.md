# Circuit to integrate with mpu6050

## ESP32

| ESP32 pins  | mpu6050 pins |
| ----------  | ------------ |
| D2*         |   INT (interrupt) |
| D21         |   SDA             |
| D22         |   SCL             |
| 3V3         |   VCC             |
| GND         |   GND             |


> \* **Note**:  Interrupt pin could be any other pin
