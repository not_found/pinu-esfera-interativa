# Circuit to read capacitive sensor

## ESP32

### Components

| Component label  | Description |
| ---------------  | ------------ |
| Cap1             |   The capacitive sensor. |
| R1               |   A resistor used at the 'send pin'  (tested with 300 KOhm). |


### Circuit:

```
SendPin -->   R1  --> +Cap1- --> GND
RcvPin         <--/
```

> **Note**:  When using esp32 'native' capacitive sensor, the SendPin is not needed.

### Pins connection

| ESP32 pins  | Role            |
| ----------  | --------------- |
| D15         |   SendPin       |
| D4          |   RcvPin        |

> **Note**:  When using esp32 'native' capacitive sensor, not all pins can be used as touch input.
