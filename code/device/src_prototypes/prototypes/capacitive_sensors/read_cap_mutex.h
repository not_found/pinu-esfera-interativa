#include "Arduino.h"
#include "classes/CapSensorRaw.h"
#include "classes/CapSensorEsp32.h"

#include "CD74HC4067.h"

#if defined(ARDUINO_ARCH_ESP8266)
#define SEND_PIN D2
#define RCV_PIN D1

#elif defined(ARDUINO_ARCH_ESP32)
// #define SEND_PIN 22
// #define RCV_PIN 23

#define SEND_PIN 15
// #define RCV_PIN 2
#define RCV_PIN 4

// #define SEND_PIN 4
// #define RCV_PIN 15

#endif


#define S0_PIN 5
#define S1_PIN 17
#define S2_PIN 16
// #define S3_PIN 4
#define S3_PIN 2

CapSensorRaw c_sense = CapSensorRaw(SEND_PIN,RCV_PIN); // 10 megohm resistor between pins 4 & 2, pin 2 is sensor pin, add wire, foil
// CapSensorEsp32 c_sense = CapSensorEsp32(RCV_PIN); // 10 megohm resistor between pins 4 & 2, pin 2 is sensor pin, add wire, foil
CD74HC4067 mux = CD74HC4067(S0_PIN, S1_PIN, S2_PIN, S3_PIN);


uint count = 0;
long valueSum = 0;
long timeSum = 0;
float percentSum = 0;

long startTimer = 0;

void setup(){
  //c_sense.set_CS_AutocaL_Millis(0xFFFFFFFF); // turn off autocalibrate on channel 1 - just as an example
  Serial.begin(9600);
  Serial.println("Start!");

  c_sense.setup();

  mux.channel(15);
}

void printValues(long deltaTime, long value, long delta, float percent){
    Serial.print(deltaTime); // check on performance in milliseconds
    Serial.print('\t');

    Serial.print("\tr"); // tab character for debug window spacing
    Serial.print(value); // print sensor output 1

    Serial.print("\td");
    Serial.print(delta);

    Serial.print('\t');
    Serial.print(percent * 100);

    Serial.println();
}

void loop(){
  long start = millis();
  // if (count == 0) {
  //   startTimer = millis();
  // }

  long value = c_sense.read();
  long deltaTime = millis() - start;

  valueSum += value;
  timeSum += deltaTime;
  percentSum += c_sense.percent();

  if (++count == 10) {

    printValues(
      timeSum,
      valueSum/count,
      (valueSum/count) - c_sense.minRead(),
      percentSum/count);

    count = 0;
    timeSum = 0;
    valueSum = 0;
    percentSum = 0;
  }



  delay(10); // arbitrary delay to limit data to serial port

}
