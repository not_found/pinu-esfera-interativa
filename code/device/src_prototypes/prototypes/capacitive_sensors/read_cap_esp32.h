#include "Arduino.h"

#define TOUCH_PIN 4

void setup() {
  Serial.begin(115200);
  Serial.println("ESP32 Touch Test");
}

void loop() {
  Serial.println(touchRead(TOUCH_PIN));  // get value of Touch 0 pin = GPIO 4
  delay(10);
}
