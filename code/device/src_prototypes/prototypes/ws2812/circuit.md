# Circuit to led strip of ws2812 leds

## ESP32

| ESP32 pins  | ws2812 strip pins |
| ----------  | ----------------- |
| D25*        |   R1 -> Din       |
| VIN (5V)\*\*|   +5V             |
| GND         |   GND             |


> **R1** is recommended to be between 300 and 500 ohms.

> \* **Note**:  Could be any other pin (below 32)

> \*\* **Note**:  Should require a level shifter to level up the signal data to 70% of VIN.
>                 But worked on tests.
