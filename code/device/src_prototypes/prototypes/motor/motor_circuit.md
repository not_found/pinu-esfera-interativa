# Circuit to use vibration motor

## ESP32

### Components

| Component label  | Description |
| ----------  | ------------ |
| T1          |   An NPN transistor (used 2N2222A transistor). |
| M1          |   The vibration motor.         |
| D1          |   An diode to use for protection of reverse currents from the motor. |
| R1          |   A resistor (tested with 1 KOhm) used to protect the ESP from over-voltage (in the transistor base). |
| R2          |   A resistor to limit the current to the motor (tested with 120 Ohm). Problably unnecessary. |


### Circuit:

```
D32* -->   R1           -> T1.base
VCC  -->   M1   ---> R2 -> T1.collector
     \->  -D1** -/
                           T1.emitter   -> GND
```

> \*   **Note**:  Can be any digital (output) pin.

> \*\* **Note**:  The diode (D1) should be reversed relative to the motor.


### Pins connection

| ESP32 pins  | components   | Transistor
| ----------  | --------------------- | ------------- |
| D32*        |   R1                  |  T1.base      |
| VCC         |   M1 -> R2            |  T1.collector |
| VCC         |   D1 (reversed) -> R2 |  T1.collector |
| GND         |                       |  T1.emmiter   |
