#include "Arduino.h"

//referencees:
// - https://techtutorialsx.com/2017/07/01/esp32-arduino-controlling-a-buzzer-with-pwm/
// - https://www.instructables.com/id/How-to-use-a-Buzzer-Arduino-Tutorial/

#define OUT_PIN 12

int freq = 2000;
int channel = 0;
int resolution = 8;

void setup() {
  Serial.begin(115200);
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(OUT_PIN, channel);
}

void loop() {

  ledcWriteTone(channel, freq);
  delay(300);
  ledcWriteTone(channel, freq);
  delay(300);

  ledcWrite(channel, 0);
  delay(2000);
}
