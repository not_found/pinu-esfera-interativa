#ifndef CAP_SENSOR_ESP32_H
#define CAP_SENSOR_ESP32_H

#include "Arduino.h"

class CapSensorEsp32{
    int rcvPin;
    long lastValue, minValue, maxValue;

public:
    CapSensorEsp32(int rcvPin)
        : rcvPin(rcvPin)
        , minValue(0x0FFFFFFFL)
    {
        maxValue=-minValue;
    }

    void setup(){
        pinMode(rcvPin, INPUT);
    }

    int read(){
        long value = touchRead(rcvPin);

        if (value < minValue) {
          minValue = value;
        }

        if (value > maxValue) {
          maxValue = value;
        }

        lastValue = value;

        return raw();
    }

    long raw(){
        return lastValue;
    }

    long minRead() const{ return minValue; }
    long maxRead() const{ return maxValue; }

    long delta(){
        return  (lastValue - minValue);
    }

    long range(){
        return (maxValue - minValue);
    }

    float percent(){
        return  delta()/(float)range();
    }
};

#endif
