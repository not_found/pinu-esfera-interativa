#ifndef MUTEX_SAMPLE_H
#define MUTEX_SAMPLE_H

#include "Arduino.h"

#include "CD74HC4067.h"

class MultiplexerWriteAll{
    // int sPins[4];
    int sig;

    bool isHigh=false;
    int lastSelected=15;

    CD74HC4067 mux;
public:
    MultiplexerWriteAll(int s0, int s1, int s2,int s3) : mux(s0, s1, s2, s3)
    {}

    void setup(int sig){
        this->sig = sig;

        // for (size_t i = 0; i < 4; i++) {
        //     pinMode(sPins[i], OUTPUT);
        // }

        pinMode(sig, OUTPUT);

        selectPin(0);
        writeValue(1);
        lastSelected = 15;
    }

    void update(){
        // isHigh = !isHigh;

        // for(uint8_t i=0; i < 16; ++i){
        //     selectPin(i);
        //
        //     // Serial.print(i);
        //     // Serial.print(": ");
        //     // Serial.print(readValue());
        //     // Serial.print("; ");
        //     writeValue();
        // }
        // // Serial.println();

        // selectPin(lastSelected);
        // writeValue(LOW);

        lastSelected = (lastSelected+1) % 16;

        selectPin(lastSelected);
        // writeValue(HIGH);
    }

    void selectPin(uint8_t number){
        // Serial.print("Selection("); Serial.print(number); Serial.print("): ");
        //
        // for (size_t i = 0; i < 4; i++) {
        //     uint8_t bit_value = ((1<<i) & number) ? HIGH : LOW;
        //     Serial.print(bit_value); Serial.print("; ");
        //     digitalWrite(sPins[i], bit_value);
        // }
        // Serial.println();

        mux.channel(number);
    }

    void toggleValue(){
        isHigh = !isHigh;

        writeValue((isHigh? HIGH : LOW));
    }

    void writeValue(int value){
        digitalWrite(sig, value);
    }
};

#endif
