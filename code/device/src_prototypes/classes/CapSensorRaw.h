#ifndef CAP_SENSOR_RAW_H
#define CAP_SENSOR_RAW_H

#include "Arduino.h"
#include <CapacitiveSensor.h>

#include "classes/FastRunningMedian.h"

class ExpFilter{
    float weight;
    long accumulated;
public:
    ExpFilter(float weight)
        : weight(weight)
        , accumulated(0)
    {}

    void reset(long value){
        accumulated = value;
    }

    long update(long value){
        accumulated = (weight*value) + (1-weight)*accumulated;

        return current();
    }

    long current(){
        return accumulated;
    }
};

class RunningAverage{
    // long average;
    long measures[10];
    int counter;
    int measuresLenght;
    long currentSum;
public:
    RunningAverage()
        : measuresLenght(10)
    {
        reset();
    }

    void reset(){
        // average = 0;
        counter = 0;
        currentSum = 0;

        for (size_t i = 0; i < measuresLenght; i++) {
            measures[i] = 0;
        }

    }

    long update(long value){
        updateAverage(value, measures[counter]);

        measures[counter] = value;
        counter = (counter + 1) % measuresLenght;

        return current();
    }

    long current(){
        return currentSum / measuresLenght;
    }
protected:
    void updateAverage(long newValue, long removedValue){
        currentSum += newValue - removedValue;
    }
};

class CapSensorRaw{
    CapacitiveSensor c_sense;
    int timeoutMillis;

    long lastValue, minValue, maxValue;

    int numSamples, readCounter;
    long readSum;

    ExpFilter filter;
    RunningAverage runningAverage;

    FastRunningMedian<long, 10, 0> runningMedian;

public:
    CapSensorRaw(int sendPin, int rcvPin, int timeoutMillis = 30, int numSamples=5)
        : c_sense(sendPin, rcvPin)
        , timeoutMillis(timeoutMillis)
        , minValue(0x0FFFFFFFL)
        , numSamples(numSamples)
        , readCounter(0)
        , readSum(0)
        , filter(0.7f)
    {
        maxValue=-minValue;

        c_sense.set_CS_Timeout_Millis(timeoutMillis);
    }

    void setup(){
        filter.reset(readSample());
    }

    // bool available(){
    //
    // }

    int read(){
        long value = filter.update(readSample());
        long average = runningAverage.update(value);
        runningMedian.addValue(value);

        if (readCounter++ > numSamples) {
            // updateRange(average);
            updateRange(runningMedian.getMedian());
            readCounter = 0;
        }


        lastValue = value;

        return raw();
    }

    long raw(){
        return lastValue;
    }

    long minRead() const{ return minValue; }
    long maxRead() const{ return maxValue; }

    long delta(){
        return  (lastValue - minValue);
    }

    long range(){
        return (maxValue - minValue);
    }

    float percent(){
        return  delta()/(float)range();
    }

protected:

    long readSample(){
        return c_sense.capacitiveSensorRaw(30);
    }

    void updateRange(int value){
        if (value < minValue) {
          minValue = value;
        }

        if (value > maxValue) {
          maxValue = value;
        }
    }
};

#endif
