#include <ArduinoFramework.h>

#include <SPI.h> //required by NeoPixelsBus
#include <BluetoothSerial.h>

#include "CapPressureServices.h"
#include "GyroscopeServices.h"
#include "BuzzerSoundServices.h"
#include "VibrationMotor.h"
#include "NeoPixelLedServices.h"

#include "SphereServices.h"

#include "SphereApi.h"

#include "BoardSensors.h"
#include "BoardPinout.h"
#include "BoardConfig.h"

#include "log.h"

GyroscopeServices gyroscopeServices;
CapPressureServices pressureServices{std::move(boardSensors())};
BuzzerSoundServices buzzer{BUZZER_PIN, BUZZER_CHANNEL};
VibrationMotor vibrationMotor{VIBRATION_PIN};
NeoPixelLedServices<> ledServices{LEDRGB_COUNT, LEDRGB_PIN};

SphereServices sphereServices{{
    &ledServices,
    &vibrationMotor,
    &buzzer,
    &gyroscopeServices,
    &pressureServices
}};

BluetoothSerial btSerial;
api::SphereApi sphereApi{&btSerial, sphereServices};
// api::SphereApi sphereApi{&Serial, sphereServices};

bool connected=false;

void setup() {
    Serial.begin(115200);
    Serial.println("starting...");

    // Serial.setDebugOutput(true);
    // esp_log_level_set("*", ESP_LOG_VERBOSE);

    if(btSerial.begin(DEFAULT_DEVICE_NAME)){
        Serial.println("Bluetooth API started");
    }

    sphereServices.begin();
    sphereApi.setup();
}

void loop() {
    if (btSerial.hasClient()){
    // if (Serial){
        connected = true;
        sphereApi.update();
    }
    else if(connected){//disconnected now
        connected = false;
        sphereApi.disconnected();
    }

    sphereServices.update();
}
