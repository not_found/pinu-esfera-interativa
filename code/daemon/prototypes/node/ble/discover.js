// var noble = require('noble');
var noble = require('@abandonware/noble');
const async = require('async')

var peripheralNameOrAddress = (process.argv[2]) ? process.argv[2].toLowerCase() : undefined;
var lookingForDevice = true;

console.log(peripheralNameOrAddress);

noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    console.log('start scanning');
    noble.startScanning();
  } else {
    console.log('stop scanning');
    noble.stopScanning();
  }
});

noble.on('discover', function(peripheral) {
    if (!lookingForDevice) {
        console.log('discovered peripheral after stop scanning');
        return;
    }

    printPeripheral(peripheral);

    if (!peripheralNameOrAddress) {
        return;
    }

    name = peripheral.advertisement.localName ? peripheral.advertisement.localName.toLowerCase() : undefined;
    addr = peripheral.address.toLowerCase();

    // console.log(name);
    // console.log(addr);
    // console.log();

    if (name === peripheralNameOrAddress
        ||  addr === peripheralNameOrAddress)
    {
        lookingForDevice = false;

        console.log('peripheral with name "' + name + '" and address "' + addr + '" found');
        noble.stopScanning();

        explore(peripheral);
    }
});

function printPeripheral(peripheral) {
    console.log('peripheral discovered (' + peripheral.id +
                ' with address <' + peripheral.address +  ', ' + peripheral.addressType + '>,' +
                ' connectable ' + peripheral.connectable + ',' +
                ' RSSI ' + peripheral.rssi + ':');
    console.log('\thello my local name is:');
    console.log('\t\t' + peripheral.advertisement.localName);
    console.log('\tcan I interest you in any of the following advertised services:');
    console.log('\t\t' + JSON.stringify(peripheral.advertisement.serviceUuids));

    var serviceData = peripheral.advertisement.serviceData;
    if (serviceData && serviceData.length) {
      console.log('\there is my service data:');
      for (var i in serviceData) {
        console.log('\t\t' + JSON.stringify(serviceData[i].uuid) + ': ' + JSON.stringify(serviceData[i].data.toString('hex')));
      }
    }
    if (peripheral.advertisement.manufacturerData) {
      console.log('\there is my manufacturer data:');
      console.log('\t\t' + JSON.stringify(peripheral.advertisement.manufacturerData.toString('hex')));
    }
    if (peripheral.advertisement.txPowerLevel !== undefined) {
      console.log('\tmy TX power level is:');
      console.log('\t\t' + peripheral.advertisement.txPowerLevel);
    }

    console.log();
}

function explore(peripheral) {
  console.log('explore');

  peripheral.once('disconnect', function() {
      console.log('Peripheral disconnected!');
    process.exit(0);
  });

  peripheral.connect(function(error) {
      if (error) {
          console.log('Connection error:');
          console.log(error);
      }
      else {
          console.log('Connected!');
      }

      // peripheral.discoverAllServicesAndCharacteristics(function (error, services, characteristics) {
      peripheral.discoverServices([], function(error, services){
         console.log('discovered services');

         if (error) {
             console.error(error);
             return;
         }

         console.log('services:');
         console.log(services);
         console.log();

         for (s of services) {
             const srv = s;
             srv.discoverCharacteristics([], function (error, characteristics) {
                console.log(`Characteristics for service ${srv.uuid}:`);
                console.log(characteristics);
             });
         }
      });

    //
    // peripheral.discoverServices([], function(error, services) {
    //     if (error) {
    //         console.error("Failed to discover services:");
    //         console.error(error);
    //     }
    //     else {
    //         console.log('Found services: ');
    //         console.log(services);
    //     }
    //   var serviceIndex = 0;
    //
    //   async.whilst(
    //     function () {
    //         console.log(`serviceIndex(${serviceIndex}) < services.length(${services.length})`);
    //       return (serviceIndex < services.length);
    //     },
    //     function(callback) {
    //         console.log('callback');
    //       var service = services[serviceIndex];
    //       var serviceInfo = service.uuid;
    //
    //       if (service.name) {
    //         serviceInfo += ' (' + service.name + ')';
    //       }
    //       console.log(serviceInfo);
    //
    //       ++serviceIndex;
    //       callback();
    //
    //       // service.discoverCharacteristics([], function(error, characteristics) {
    //       //   var characteristicIndex = 0;
    //       //
    //       //   async.whilst(
    //       //     function () {
    //       //       return (characteristicIndex < characteristics.length);
    //       //     },
    //       //     function(callback) {
    //       //       var characteristic = characteristics[characteristicIndex];
    //       //       var characteristicInfo = '  ' + characteristic.uuid;
    //       //
    //       //       if (characteristic.name) {
    //       //         characteristicInfo += ' (' + characteristic.name + ')';
    //       //       }
    //       //
    //       //       async.series([
    //       //         function(callback) {
    //       //           characteristic.discoverDescriptors(function(error, descriptors) {
    //       //             async.detect(
    //       //               descriptors,
    //       //               function(descriptor, callback) {
    //       //                 if (descriptor.uuid === '2901') {
    //       //                   return callback(descriptor);
    //       //                 } else {
    //       //                   return callback();
    //       //                 }
    //       //               },
    //       //               function(userDescriptionDescriptor){
    //       //                 if (userDescriptionDescriptor) {
    //       //                   userDescriptionDescriptor.readValue(function(error, data) {
    //       //                     if (data) {
    //       //                       characteristicInfo += ' (' + data.toString() + ')';
    //       //                     }
    //       //                     callback();
    //       //                   });
    //       //                 } else {
    //       //                   callback();
    //       //                 }
    //       //               }
    //       //             );
    //       //           });
    //       //         },
    //       //         function(callback) {
    //       //               characteristicInfo += '\n    properties  ' + characteristic.properties.join(', ');
    //       //
    //       //           if (characteristic.properties.indexOf('read') !== -1) {
    //       //             characteristic.read(function(error, data) {
    //       //               if (data) {
    //       //                 var string = data.toString('ascii');
    //       //
    //       //                 characteristicInfo += '\n    value       ' + data.toString('hex') + ' | \'' + string + '\'';
    //       //               }
    //       //               callback();
    //       //             });
    //       //           } else {
    //       //             callback();
    //       //           }
    //       //         },
    //       //         function() {
    //       //           console.log(characteristicInfo);
    //       //           characteristicIndex++;
    //       //           callback();
    //       //         }
    //       //       ]);
    //       //     },
    //       //     function(error) {
    //       //       serviceIndex++;
    //       //       callback();
    //       //     }
    //       //   );
    //       // });
    //
    //
    //     },
    //     function (err) {
    //         console.error(err);
    //       peripheral.disconnect();
    //     }
    //   );
    // });

  });
}
