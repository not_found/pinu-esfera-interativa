import bluetooth

def connect_to(device_addr, port=1):
    print('connecting')

    sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
    sock.connect((device_addr, port))

    sock.send("hello!!")

    sock.close()

def main():
    import sys

    addr = sys.argv[1]

    connect_to(addr)

if __name__ == '__main__':
    main()
