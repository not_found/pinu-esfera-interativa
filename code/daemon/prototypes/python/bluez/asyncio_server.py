import asyncio
from socket import SOCK_STREAM, AF_BLUETOOTH
import bluetooth

@asyncio.coroutine
def connect_client(loop, sock):
    client, addr = yield from loop.sock_accept(sock)
#    client.type = SOCK_STREAM
#    client.family = AF_BLUETOOTH
    transport, protocol = yield from loop.connect_accepted_socket(asyncio.Protocol, client)

loop = asyncio.get_event_loop()
bt_sock = bluetooth.BluetoothSocket()
bt_sock.bind(("",bluetooth.PORT_ANY))
bt_sock.settimeout(0)
bt_sock.listen(1)

loop.run_until_complete(connect_client(loop, bt_sock))
