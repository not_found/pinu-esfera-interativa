import asyncio
import array
import sys
from bleak import discover
from bleak import BleakClient

address = "dd:0c:e4:29:32:ab"
UUID_NORDIC_TX = "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
UUID_NORDIC_RX = "6e400003-b5a3-f393-e0a9-e50e24dcca9e"

def uart_data_received(sender, data):
    print("RX> {0}".format(data))

# You can scan for devices with:
async def run():
   devices = await discover()

   d = select_device(devices)

   if d is None:
       return

   async with BleakClient(d.address, loop=loop) as client:
       x = await client.is_connected()
       print("Connected: {0}".format(x))

       charac_uuid = sys.argv[2] if len(sys.argv) > 2 else None
       if charac_uuid:
           await client.start_notify(charac_uuid, uart_data_received)

           print("Waiting for data")
           await asyncio.sleep(5.0, loop=loop) # wait for a response
           print("Done!")

def select_device(devices):
    name = sys.argv[1] if len(sys.argv) > 1 else None
    print(name)

    device = None

    for d in devices:
        print('{}: {}'.format(d.name, d.address))

        if d.name == name:
            return d

    return None

# print("Connecting...")
# async def run(address, loop):
#     async with BleakClient(address, loop=loop) as client:
#         print("Connected")
#         await client.start_notify(UUID_NORDIC_RX, uart_data_received)
#         print("Writing command")
#         c=command
#         while len(c)>0:
#           await client.write_gatt_char(UUID_NORDIC_TX, bytearray(c[0:20]), True)
#           c = c[20:]
#         print("Waiting for data")
#         await asyncio.sleep(1.0, loop=loop) # wait for a response
#         print("Done!")


loop = asyncio.get_event_loop()
loop.run_until_complete(run())
