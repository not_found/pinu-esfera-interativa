import asyncio
import logging

from api import BluetoothApi

LOG = logging.getLogger(__name__)


async def main():
    client = BluetoothApi()

    has_finished = False

    while not has_finished:
        print("discovering")
        sphere = await client.discover('sphere')

        print(sphere)

        stdin_r = await stdin_reader()

        print("connecting")
        await sphere.connect()
        print("connected")

        t1 = from_reader_to_sdout(sphere.reader)
        t2 = pipe_reader_to_writer(stdin_r, sphere.writer)

        await asyncio.wait([t1, t2], return_when=asyncio.FIRST_COMPLETED)

        has_finished = stdin_r.at_eof() or sphere.reader.at_eof()

        if has_finished:
            print("finished")
        else:
            print("reconnecting")
            await asyncio.sleep(1)

async def from_reader_to_sdout(reader):
    from sys import stdout

    try:
        loop = asyncio.get_event_loop()

        while loop.is_running() and not reader.at_eof():
            # data = await reader.readline()
            data = await reader.read(1)
            if not data:
                break

            # print(data.decode())
            stdout.write(data.decode())
            stdout.flush()
    except Exception as e:
        print("stopped reader due to: {}".format(e))

async def pipe_reader_to_writer(reader, writer):
    # import sys
    #
    # loop = asyncio.get_event_loop()
    #
    # reader = asyncio.StreamReader(loop=loop)
    # await loop.connect_read_pipe(
    #     lambda: asyncio.StreamReaderProtocol(reader, loop=loop), sys.stdin)

    try:
        while loop.is_running() and not reader.at_eof():
            data = await reader.readline()
            if not data:
                break

            writer.write(data)
            await writer.drain()
    except Exception as e:
        print("stopped writer due to: {}".format(e))
        return


async def stdin_reader():
    import sys

    loop = asyncio.get_event_loop()

    reader = asyncio.StreamReader(loop=loop)
    await loop.connect_read_pipe(
        lambda: asyncio.StreamReaderProtocol(reader, loop=loop), sys.stdin)

    return reader




def on_loop_exception(loop, context):
    exc_msg = context.get('message')
    exc = context.get('exception')

    LOG.debug('caught exception in loop: "{}"'.format(exc_msg), exc_info=exc)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    loop.set_exception_handler(on_loop_exception)
    # loop.add_reader(d, on_ready, d)
    loop.run_until_complete(main())
