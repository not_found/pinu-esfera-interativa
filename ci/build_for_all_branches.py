#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import subprocess

def main():
    valid_branches = []

    for b in list_remote_branches():
        checkout_to(b)
        try:
            if run_build(b):
                valid_branches.append(b)
        finally:
            checkout_back()

    generate_index(valid_branches, 'public')

def run_build(r_branch):
    import shutil
    from os import path

    if not path.isfile('mkdocs.yml'):
        eprint('{}: not found "mkdocs.yml" file!'.format(r_branch))
        return False
    else:
        print('{}: building docs...'.format(r_branch))

    b           = r_branch.split('/')[1]
    site_dir    = path.abspath(b)
    public_dir  = path.abspath('public')
    public_site = path.join(public_dir, b)

    run('mkdocs build -q --site-dir={}'.format(site_dir))
    try_create_dir(public_dir)
    shutil.move(site_dir, public_site)

    print('{}: build finished!'.format(r_branch))

    return True

def generate_index(branches, out):
    from os import path


    branches_list = [gen_branch_list_item(b) for b in branches]

    content = '''
<html>
<head>
    <meta charset="utf-8"/>
</head>
<body>
    <h1>Branches:</h1>
    <table>
        <tr>
            <th align="left">branch</th>
            <th>última atualização</th>
        </tr>
        {}
    </table>

    <style>
        .commit {{
          display:flex;
          justify-content: space-between;
        }}
        .commit *{{
            margin-left: 30px
        }}
        .commit :first-child {{
            margin-left: 0px
        }}
    </style>
</body>
</html>
    '''.format('\n'.join(branches_list))

    with open(path.join(out, 'index.html'), 'w') as f:
        f.write(content)

def gen_branch_list_item(b):
    b_name = b.split('/')[1]

    c_hash = get_commit_hash(b)
    c_date = get_commit_date(b)

    return '''
        <tr>
            <td>
                <a href="{0}/index.html" class="commit">
                    <span>{0}</span>
                    <span>[{1}]</span>
                </a>
            </td>
            <td>
                <time datetime="{2}">
                    <script>
                        document.write((new Date('{2}')).toLocaleDateString())
                    </script>
                </time>
            </td>
        </tr>
'''.format(b_name, c_hash, c_date)

def list_remote_branches():
    out = run_decode('git branch -r')

    branches = out.strip().split();

    return [b for b in branches if b.startswith('origin/')]

def get_commit_hash(b):
    return run_decode('git rev-parse --short {}'.format(b))

def get_commit_date(b):
    from datetime import datetime

    return run_decode('git show -s --format=%cd {}'.format(b)).strip()

def checkout_to(branch):
    run('git checkout -q {}'.format(branch))

def checkout_back():
    try:
        checkout_to('-')
    except Exception as e:
        eprint('Failed to return to previous commit, due to: %s' % e)

def run_decode(cmd, *args, **kwargs):
    return run(cmd, *args, **kwargs).decode('utf-8')

def run(cmd, *args, **kwargs):
    return subprocess.check_output(cmd.split(), *args, **kwargs)

def try_create_dir(directory):
    import os, errno

    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise e

def eprint(*args, **kwargs):
    import sys

    print(*args, file=sys.stderr, **kwargs)

if __name__ == '__main__':
    main()
