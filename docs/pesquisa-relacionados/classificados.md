# Classificados


## Prováveis relacionados

### Técnicas







------------------------------------------------------------------------------------------

## Possíveis relacionados

### Controles | Input

#### Deformáveis

* Bächer, M., B. Hepp, F. Pece, P.G. Kry, B. Bickel, B. Thomaszewski, e O. Hilliges. “DefSense: Computational design of customized deformable input devices”, 3806–16, 2016. https://doi.org/10.1145/2858036.2858354.

* Grierson, M., e C. Kiefer. “NoiseBear: A Wireless Malleable Multiparametric Controller for use in Assistive Technology Contexts”, 2013-April:2923–26, 2013. https://doi.org/10.1145/2468356.2479575.
    - controle; deformável
    - > NoiseBear is a malleable multiparametric interface
    - > The system is a highly sensitive deformable controller; it can be used flexibly in a range of scenarios for continuous or discrete control, allowing interaction to be designed at a range of complexity levels.
    - > controller is wireless, and can be used to extend the interactive possibilities of mobile computing devices.

* Lee, S.-S., S. Kim, B. Jin, E. Choi, B. Kim, X. Jia, D. Kim, e K.-P. Lee. “How users manipulate deformable displays as input devices”, 3:1647–56, 2010. https://doi.org/10.1145/1753326.1753572.
    - > This study is aimed at understanding deformation-based user gestures by observing users interacting with artificial deformable displays with various levels of flexibility.
    - > We gained user-defined gestures that would help with the design and implementation of deformation-based interface, without considering current technical limitations.

#### Tangíveis

* De Miranda, L., H.H. Hornung, e M.C.C. Baranauskas. “Adjustable interactive rings for iDTV”. IEEE Transactions on Consumer Electronics 56, nº 3 (2010): 1988–96. https://doi.org/10.1109/TCE.2010.5606356.
    - dispositivo: aneis digitais interativos

#### Gestuais

* Matulic, F., C. Träger, L. Engeln, e R. Dachselt. “Embodied interactions for novel immersive presentational experiences”, 07-12-May-2016:1713–20, 2016. https://doi.org/10.1145/2851581.2892501.
    - > we introduce and propose a preliminary realisation of a concept to enhance live multimedia presentations, where presenters are directly integrated in their presentation content as interactive avatars.
    - > Using multimodal input, especially body gestures, presenters control those embedded avatars through which they can interact with the virtual presentation environment in a fine-grained fashion

* Mohr, P., M. Tatzgern, T. Langlotz, A. Lang, D. Schmalstieg, e D. Kalkofen. “TrackCap: Enabling smartphones for 3D interaction on mobile head-mounted displays”, 2019. https://doi.org/10.1145/3290605.3300815.
        - > TrackCap, a novel approach for 3D tracking of input devices, turns a conventional smartphone into a precise 6DOF input device for an HMD user. The device can be conveniently operated both inside and outside the HMD's field of view, while it provides additional 2D input and output capabilities.

### Técnicas e tecnologias

* Baudisch, P., e G. Chu. “Back-of-device interaction allows creating very small touch devices”, 1923–32, 2009. https://doi.org/10.1145/1518701.1518995.

#### pressure based

* Besançon, L., M. Ammi, e T. Isenberg. “Pressure-based gain factor control for mobile 3D interaction using locally-coupled devices”, 2017-May:1831–42, 2017. https://doi.org/10.1145/3025453.3025890.
    - Uso de "aperto"(?) para controlar objetos 3D
    - > design and evaluation of pressure-based interactive control of 3D navigation precision.
    - > We present two experiments.
        - > First, we determined that people strongly preferred higher pressures to be mapped to higher gain factors.
        - > we compared pressure with [...] Our results show that pressure-based gain control allows people to be more precise in the same amount of time compared to established input modalities.

* Cechanowicz, J., P. Irani, e S. Subramanian. “Augmenting the mouse with pressure sensitive input”, 1385–94, 2007. https://doi.org/10.1145/1240624.1240835.
    - > [Proposta] we investigate the use of a uni-pressure and dual-pressure augmented mouse.
        - > Two or more independent pressure sensors can be mounted onto several locations on the body of the mouse.
    - > we conducted a multi-part study
        - > we identified the number of maximum discrete levels controllable with a uni-pressure augmented mouse, the most appropriate locations for installing pressure sensors on the mouse, and the design of new interaction techniques to support selection with pressure-based input.
        - > Our results show that users can comfortably control up to 64 modes with a dual-pressure augmented mouse.
        - > We discuss the findings of our results in the context of several desktop interaction techniques and identify several design recommendations.

* Stewart, C., M. Rohs, S. Kratz, e G. Essl. “Characteristics of pressure-based input for mobile devices”, 2:801–10, 2010. https://doi.org/10.1145/1753326.1753444.


#### Interação gestual


* Chan, L., C.-H. Hsieh, Y.-L. Chen, S. Yang, D.-Y. Huang, R.-H. Liang, e B.-Y. Chen. “Cyclops: Wearable and single-piece full-body gesture input devices”, 2015-April:3001–10, 2015. https://doi.org/10.1145/2702123.2702464.
    - interação: gestual (via câmera)
    - relação: pode servir p/ detectar gestos com esfera (?)
    - > This paper presents Cyclops, a single-piece wearable device that sees its user's whole body postures through an ego-centric view of the user that is obtained through a fisheye lens at the center of the user's body, allowing it to see only the user's limbs and interpret body postures effectively.

* Chen, K.-Y., S. Patel, e S. Keller. “Finexus: Tracking precise motions of multiple fingertips using magnetic sensing”, 1504–14, 2016. https://doi.org/10.1145/2858036.2858125.
    - interação: gestual (via sensores magnéticos)
    - > By instrumenting the fingertips with electromagnets, the system can track fine fingertip movements in real time using only four magnetic sensors.
    - > We develop a novel algorithm to efficiently calculate the 3D positions of multiple electromagnets from corresponding field strengths.
    - > Finexus is applicable to a wide variety of human input tasks, such as writing in the air.

* Corenthy, L., M. Giordano, R. Hayden, D. Griffiths, C. Jeffrey, H. Limerick, O. Georgiou, T. Carter, J. Müller, e S. Subramanian. “Touchless tactile displays for digital signage: mid-air haptics meets large screens”, Vol. 2018-April, 2018. https://doi.org/10.1145/3170427.3186533.
    - > This demo presents the concept of Interactive Digital Signage with Haptics, where users can interact with public digital screens with their bare hands, utilizing tracking technology and ultrasonic mid-air haptic feedback.

* Moeller, J., A. Kerne, e S. Damaraju. “ZeroTouch: A zero-thickness optical multi-touch force field”, 1165–70, 2011. https://doi.org/10.1145/1979742.1979710.
    - gestual (?); toque (?)
    - > We present zero-thickness optical multi-touch sensing, a technique that simplifies sensordisplay integration, and enables new forms of interaction not previously possible with other multi-touch sensing techniques.
    - > Using low-cost modulated infrared sensors to quickly determine the visual hull of an interactive area, we enable robust real-time sensing of fingers and hands, even in the presence of strong ambient lighting.

* Tan, Y., S.H. Yoon, e K. Ramani. “Bikegesture: User elicitation and performance of micro hand gesture as input for cycling”, Part F127655:2147–54, 2017. https://doi.org/10.1145/3027063.3053075.
    - interação: gestual
    - > In this paper, we investigate the acceptance and performance of using hand gesture during cycling.
    - > Through an observational study with 16 users, we devised a taxonomy of hand gestures.
        - > Users prefer subtle micro hand gestures to ensure safe cycling while maintaining a flexible controllability.
    - > We also implemented a wearable prototype that recognizes these gestures.

#### Feedback mecânico

* Fukumoto, M. “PuyoSheet and PuyoDots: Simple techniques for adding ‘button- push’ feeling to touch panels”, 3925–30, 2009. https://doi.org/10.1145/1520340.1520595.
    - > Two simple techniques for touch-panel based portable information devices are proposed.
        - > A soft-gel based transparent film named "PuyoSheet" placed over a touch panel provides button-push feeling to the fingertips.
        - > Another configuration, soft-gel based small dots, named "PuyoDots", is attached to the backside of a handheld device provides button-edge and button-push feelings to the fingertip(s) that hold the device.

------------------------------------------------------------------------------------------

## Talvez relacionados

### Esferas

* Heberlein, M., T. Hayashi, S. Nashold, e S. Teeravarunyou. “CHI-ball, an interactive device assisting martial arts education for children”, 962–63, 2003. https://doi.org/10.1145/765891.766095.
    - esfera


### Devices / sensores

#### Cordas

* Shahmiri, F., C. Chen, A. Waghmare, D. Zhang, S. Mittal, S.L. Zhang, Y.-C. Wang, Z.L. Wang, T.E. Starner, e G.D. Abowd. “Serpentine: A reversibly deformable cord sensor for human input”, 2019. https://doi.org/10.1145/3290605.3300775.
    - > We introduce Serpentine, a self-powered sensor that is a reversibly deformable cord capable of sensing a variety of human input. The material properties and structural design of Serpentine allow it to be flexible, twistable, stretchable and squeezable, enabling a broad variety of expressive input modalities.

* Schwarz, J., C. Harrison, S. Hudson, e J. Mankoff. “Cord input: An intuitive, high-accuracy, multi-degree-of-freedom input method for mobile devices”, 3:1657–60, 2010. https://doi.org/10.1145/1753326.1753573.
    - > In this note, we describe a proof-of-concept cord-based sensor, which senses three of the four input dimensions we propose.

#### Moldáveis

* Rudeck, F., e P. Baudisch. “Rock-Paper-Fibers: Bringing physical affordance to mobile touch devices”, 1929–32, 2012. https://doi.org/10.1145/2207676.2208334.
    - deformáveis
    - > We explore how to bring physical affordance to mobile touch devices.
    - > We present Rock-Paper-Fibers, a device that is functionally equivalent to a touchpad, yet that users can reshape so as to best match the interaction at hand.


### Técnicas e tecnologias

#### Interação tangível

* Cherek, C., S. Voelker, D. Asselborn, e J. Borchers. “Off-surface tangibles: Exploring the design space of midair tangible interaction”, 2019. https://doi.org/10.1145/3290607.3312966.
    - > We introduce the interaction concept of Off-Surface Tangibles that are tracked by the surface but continue to support meaningful interactions when lifted off the surface.
    - > We populate the design space with prior work and introduce possible interaction designs for further research.

* Ikematsu, K., M. Fukumoto, e I. Siio. “Ohmic-Sticker: Force-to-motion type input device for capacitive touch surface”, 2019. https://doi.org/10.1145/3290607.3312936.
    - > We propose "Ohmic-Sticker", a novel force-to-motion type input device to extend capacitive touch surfaces.

#### Interação por toque

* Grau, A., C. Hendee, J.-R. Rizzo, e K. Perlin. “Mechanical force redistribution: Enabling seamless, large-format, high-accuracy surface interaction”, 4137–46, 2014. https://doi.org/10.1145/2556288.2557172.
    - > We present Mechanical Force Redistribution (MFR): a method of sensing which creates an anti-aliased image of forces applied to a surface.
        - > This technique mechanically focuses the force from a surface onto adjacent discrete forcels (force sensing cells) by way of protrusions (small bumps or pegs), allowing for high-accuracy interpolation between adjacent discrete forcels

* “GhostID: Enabling non-persistent user differentiation in frequency-division capacitive multi-touch sensors”, 2017-May:15–27, 2017. https://doi.org/10.1145/3025453.3025719.
    - > We present GhostID, a capacitive sensor that can differentiate the origins of multiple simultaneous touches.

#### Pressure based

* Ramos, G., M. Boulos, e R. Balakrishnan. “Pressure widgets”, 487–94, 2004. https://dl.acm.org/citation.cfm?id=985754
    - > We explore the design space of using the continuous pressure sensing capabilities of styluses

#### 'output'

* D’Alessandro, N., B. Pritchard, J. Wang, e S. Fels. “Ubiquitous voice synthesis: Interactive manipulation of speech and singing on mobile distributed platforms”, 335–40, 2011. https://doi.org/10.1145/1979742.1979700.
    - Síntese de voz
    - > We present DiVA and HandSketch as our two current voice-based digital musical instruments.
    - > We then discuss the evolution of this performance practice into a new ubiquitous model applied to voice synthesis, and we describe our first prototype using a mobile phone and wireless embodied devices in order to allow a group of users to collaboratively produce voice synthesis in real-time.


#### Interação a distância

* Siddhpuria, S., S. Malacria, M. Nancel, e E. Lank. “Pointing at a distance with everyday smart devices”, Vol. 2018-April, 2018. https://doi.org/10.1145/3173574.3173747.
    - > We contribute (1) the results of a survey on possession and use of smart devices, and (2) the results of a controlled experiment comparing seven distal pointing techniques on phone or watch, one- and two-handed, and using different input channels and mappings.

### Software

* Tang, W.W.W., S.C.F. Chan, K.W.K. Lo, H.V. Leong, A.T.S. Chan, e G. Ngai. “i*Chameleon: A scalable and extensible framework for multimodal interaction”, 305–10, 2011. https://doi.org/10.1145/1979742.1979703.
    - > i*Chameleon is a multimodal interaction framework that enables programmers to readily prototype and test new interactive devices or interaction modes.
    - > This is made possible with the engineering of an interaction framework that distills the complexity of control processing to a set of semantically-rich modal controls that are discoverable, composable and adaptable.


### Técnicas de pesquisa

* Gerken, J., H.-J. Bieg, S. Dierdorf, e H. Reiterer. “Enhancing input device evaluation: Longitudinal approaches”, 4351–56, 2009. https://doi.org/10.1145/1520340.1520665.
    - avaliação
    - > In this paper we present our experiences with longitudinal study designs for input device evaluation.
    - > In this domain, analyzing learning is currently the main reason for applying longitudinal designs.


### Técnicas de fabricação

* Groeger, D., e J. Steimle. “LASEC: Instant fabrication of stretchable circuits using a laser cutter”, 2019. https://doi.org/10.1145/3290605.3300929.
    - diy, controle, técnica, circuito, 'stretchable'
    - > This paper introduces LASEC, the first technique for instant do-it-yourself fabrication of circuits with custom stretchability on a conventional laser cutter

-------------------------------------------------------------------------------------------------

## Improváveis

### Controles

* Nabeshima, Shinji, Shinichirou Yamamoto, Kiyoshi Agusa, e Toshio Taguchi. “MEMO-PEN: a new input device”, 2:256–57, 1995. https://dl.acm.org/citation.cfm?id=223662
    - antigo

--------------------------------------------------------------------------------------------------

## Off-topic interessantes


### Não classificados

* Ishii, H., C. Wisneski, J. Orbanes, B. Chun, e J. Paradiso. “PingPongPlus: Design of an athletic-tangible interface for computer-supported cooperative play”, 394–401, 1999. https://doi.org/10.1145/302979.303115.
    - esfera - antigo - detecção de movimento via som (?) - não é controle

* Költringer, T., M.N. Van, e T. Grechenig. “Game controller text entry with alphabetic and multi-tap selection keyboards”, 2513–18, 2007. https://doi.org/10.1145/1240866.1241033.

* Thommes, D., A. Gerlicher, Q. Wang, e C. Grecos. “RemoteUI: A high-performance remote user interface system for mobile consumer electronic devices”. IEEE Transactions on Consumer Electronics 58, nº 3 (2012): 1094–1102. https://doi.org/10.1109/TCE.2012.6311361.

* Uddin, M.S., C. Gutwin, e B. Lafreniere. “HandMark menus: Rapid Command selection and large command sets on multi-touch displays”, 5836–48, 2016. https://doi.org/10.1145/2858036.2858211.

* Vogel, D., M. Cudmore, G. Casiez, R. Balakrishnan, e L. Keliher. “Hand occlusion with tablet-sized direct pen input”, 557–66, 2009. https://doi.org/10.1145/1518701.1518787.

* Walker, J., B. Li, K. Vertanen, e S. Kuhl. “Efficient typing on a visually occluded physical keyboard”, 2017-May:5457–61, 2017. https://doi.org/10.1145/3025453.3025783.

* Williams, C., X.-D. Yang, G. Partridge, J. Usiskin-Miller, A. Major, e P. Irani. “TZee: Exploiting the lighting properties of multi-touch tabletops for tangible 3D interactions”, 1363–72, 2011. https://doi.org/10.1145/1978942.1979143.

* Wongsuphasawat, K., J.A.G. Gómez, C. Plaisant, T.D. Wang, S. Ben, e M. Taieb-Maimon. “LifeFlow: Visualizing an overview of event sequences”, 1747–56, 2011. https://doi.org/10.1145/1978942.1979196.

* Piumsomboon, T., G.A. Lee, A. Irlitti, B. Ens, B.H. Thomas, e M. Billinghurst. “On the shoulder of the giant: A multi-scale mixed reality collaboration with 360 video sharing and tangible interaction”, 2019. https://doi.org/10.1145/3290605.3300458.



### Controles

* Alonso, M.B., e D.V. Keyson. “Musiccube: Making digital music tangible”, 1176–79, 2005. https://doi.org/10.1145/1056808.1056870.
    - >  a wireless cube-like object, using gestures to shuffle music and a rotary dial with a button for song navigation and volume control.
    - > Speech and non-speech feedback were given to communicate current mode and song title.

* Ashbrook, D., P. Baudisch, e S. White. “Nenya: Subtle and eyes-free mobile input with a magnetically-tracked finger ring”, 2043–46, 2011. https://doi.org/10.1145/1978942.1979238.
    - dispositivo: anel
    - > We present Nenya, a new input device in the shape of a finger ring.
    - > Users make selections by twisting the ring and "click" by sliding it along the finger.

* Dobbelstein, D., P. Hock, e E. Rukzio. “Belt: An unobtrusive touch input device for head-worn displays”, 2015-April:2135–38, 2015. https://doi.org/10.1145/2702123.2702450.
    - dispositivo: cinto
    - > Belt is a novel unobtrusive input device for wearable displays that incorporates a touch surface encircling the user's hip.

* Froehlich, B., J. Hochstrate, V. Skuk, e A. Huckauf. “The GlobeFish and the GlobeMouse: Two new six degree of freedom input devices for graphics applications”, 1:191–99, 2006. https://dl.acm.org/citation.cfm?id=1124802
    - > We introduce two new six degree of freedom desktop input devices based on the key concept of combining forceless isotonic rotational input with force-requiring elastic translational input.

* Fröhlich, B., e J. Plate. “The cubic mouse a new device for three-dimensional input”, 526–31, 2000. https://doi.org/10.1145/332040.332491.
    - > We have developed a new input device that allows users to intuitively specify three-dimensional coordinates in graphics applications.
    - > The device consists of a cube-shaped box with three perpendicular rods passing through the center and buttons on the top for additional control. The rods represent the X, Y, and Z axes of a given coordinate system. Pushing and pulling the rods specifies constrained motion along the corresponding axes.

* Todi, K., D. Degraen, B. Berghmans, A. Faes, M. Kaminski, e K. Luyten. “Purpose-centric appropriation of everyday objects as game controllers”, 07-12-May-2016:2744–50, 2016. https://doi.org/10.1145/2851581.2892448.

* Smith, T. “Squidge: An Integrated Game Controller”, 2013-April:2651–54, 2013. https://doi.org/10.1145/2468356.2479486.

* Ogata, M., Y. Sugiura, H. Osawa, e M. Imai. “Pygmy: A ring-like anthropomorphic device that animates the human hand”, 1003–6, 2012. https://doi.org/10.1145/2212776.2212371.
    - > Pygmy is an anthropomorphic device that magnifies hand expressions. It is based on the concept of hand anthropomorphism and it uses finger movements to create the anthropomorphic effect. Wearing the device is similar to having eyes and a mouth on the hand; the wearer's hand spontaneously expresses their emotions. Interactive manipulation by controllers and sensors make the hand look animated.

* Nichols, J., B.A. Myers, e B. Rothrock. “UNIFORM: Automatically generating consistent remote control user interfaces”, 1:611–20, 2006. https://dl.acm.org/citation.cfm?id=1124865
    - > This paper presents a system, called Uniform, which approaches this problem by automatically generating remote control interfaces that take into account previous interfaces that the user has seen during the generation process.
    - > Uniform is able to automatically identify similarities between different devices and users may specify additional similarities.

#### Controle + experiência

* Berghmans, B., A. Faes, M. Kaminski, e K. Todi. “Household survival: Immersive Room-sized gaming using everyday objects as weapons”, 07-12-May-2016:168–71, 2016. https://doi.org/10.1145/2851581.2890372.


### Interações / interfaces não usuais

* Canat, M., M.O. Tezcan, C. Yurdakul, O.T. Buruk, e O. Özcan. “Experiencing human-to-human touch in digital games”, 07-12-May-2016:3655–58, 2016. https://doi.org/10.1145/2851581.2890231.

* Cascón, P.G., D.J.C. Matthies, S. Muthukumarana, e S. Nanayakkara. “Chewit. An intraoral interface for discreet interactions”, 2019. https://doi.org/10.1145/3290605.3300556.

* Harrer, S., P. Jarnfelt, e S. Nielsen. “Of mice and pants: Queering the conventional gamer mouse for cooperative play”, 2019. https://doi.org/10.1145/3290607.3310431.
    - > We then discuss three experimental game design strategies to queer the mouse controller in The Undie Game, a cooperative wearable mouse-based installation game by the Copenhagen Game Collective. The Undie Game speculates about ways to confront and disrupt conventional expectations about gaming by fa''silly''tating interaction for two players who wear a mouse controller in their panties and collectively steer a 3D high definition tongue on screen to achieve a mutual highscore.

* Hwang, S., G. Lee, B. Jeong, W. Lee, e I. Cho. “Feeltip: Tactile input device for small wearable information appliances”, 1475–78, 2005. https://doi.org/10.1145/1056808.1056945.
    - > We introduce here an input device, FeelTip, as a solution for very small information devices. The main idea is to exchange the usual roles of a finger and a surface in a touchpad; a device has a tip and a finger now provides a surface

* Jansen, Y. “Mudpad: Fluid haptics for multitouch surfaces”, 4351–56, 2010. https://doi.org/10.1145/1753846.1754152.

* Klamka, K., e R. Dachselt. “ARCord: Visually augmented interactive cords for mobile interaction”, Vol. 2018-April, 2018. https://doi.org/10.1145/3170427.3188456.
    - AR, wearable
    - > With this paper, we want to extend the interaction and application repertoire of body-worn cords by contributing the concept of visually augmented interactive cords using state-of-the-art augmented reality (AR) glasses.

* Klamka, K., e R. Dachselt. “IllumiPaper: Illuminated interactive paper”, 2017-May:5605–18, 2017. https://doi.org/10.1145/3025453.3025525.
    - 'papel interativo'
    - > In this paper, we investigate new forms of paper-integrated feedback, which build on emerging paper-based electronics and novel thin-film display technologies.
    - > As a major contribution, we present a systematic feedback repertoire for real-world applications including feedback components for innovative paper interaction tasks in five categories.
    - > Furthermore, we contribute a fully-functional research platform including a paper-controller, digital pen and illuminated, digitally controlled papers that demonstrate the feasibility of our techniques.

* Knibbe, J., J. Wu, D. Martinez, T. Cable, C. Bainbridge, H. Munir, C.-K. Chan, e D. Coyle. “Extending interaction for smart watches: Enabling bimanual around device control”, 1891–96, 2014. https://doi.org/10.1145/2559206.2581315.
    - > In this paper, we look at extending the interactive surface for a smart watch to the back of the hand.
    - > We define a range of supported bimanual gestures and present a prototype device.

* Laput, G., e C. Harrison. “Surfacesight: A new spin on touch, user, and object sensing for IoT experiences”, 2019. https://doi.org/10.1145/3290605.3300559.
    - > We describe SurfaceSight, an approach that enriches IoT experiences with rich touch and object sensing, offering a complementary input channel and increased contextual awareness.
    - > For sensing, we incorporate LIDAR into the base of IoT devices, providing an expansive, ad hoc plane of sensing just above the surface on which devices rest.
    - > We can recognize and track a wide array of objects, including finger input and hand gestures. We can also track people and estimate which way they are facing.

* Maynes-Aminzade, D., e H.S. Raffle. “You’re In Control: A urinary user interface”, 986–87, 2003. https://doi.org/10.1145/765891.766108.

* Trindade, R., M. Sousa, C. Hart, N. Vieira, R. Rodrigues, e J. França. “Purrfect crime: Exploring animal computer interaction through a digital game for humans and cats”, 18:93–96, 2015. https://doi.org/10.1145/2702613.2728660.

* Nam, H.Y., e C. Disalvo. “Tongue music: The sound of a kiss”, 4805–8, 2010. https://doi.org/10.1145/1753846.1754235.

* Ren, Z., R. Mehra, J. Coposky, e M. Lin. “Designing virtual instruments with touch-enabled interface”, 433–36, 2012. https://doi.org/10.1145/2212776.2212820.
    - tabletop

* Nenonen, V., A. Lindblad, V. Häkkinen, T. Laitinen, M. Jouhtio, e P. Hämäläinen. “Using heart rate to control an interactive game”, 853–56, 2007. https://doi.org/10.1145/1240624.1240752.

* Nafiz Hasan Khan, Md., A. Antle, e C. Neustaedter. “Flight chair: An interactive chair for controlling emergency service drones”, 2019. https://doi.org/10.1145/3290607.3313031.

#### Tangíveis

* Schmidt, D., R. Ramakers, E.W. Pedersen, J. Jasper, S. Köhler, A. Poh, H. Rantzsch, et al. “Kickables: Tangibles for feet”, 3143–52, 2014. https://doi.org/10.1145/2556288.2557016.
    - > We present a custom design as well as five families of standard kickables to help application designers create kickable applications faster. Each family supports multiple standard controls, such as push buttons, switches, dials, and sliders. Each type explores a different design principle, in particular different mechanical constraints. We demonstrate an implementation on our pressure-sensing floor.

#### Canetas / desenho

* Kazi, R.H., T. Igarashi, S. Zhao, e R.C. Davis. “Vignette: Interactive texture design and manipulation with freeform gestures for pen-and-ink illustration”, 1727–36, 2012. https://doi.org/10.1145/2207676.2208302.
    - > Vignette is an interactive system that facilitates texture creation in pen-and-ink illustrations. Unlike existing systems, Vignette preserves illustrators' workflow and style: users draw a fraction of a texture and use gestures to automatically fill regions with the texture.

* Kim, H.-J., H. Kim, T.-D. Han, J. Seo, e S. Chae. “AR Pen and Hand Gestures: A New Tool for Pen Drawings”, 2013-April:943–48, 2013. https://doi.org/10.1145/2468356.2468525.
    - > We propose a new interactive AR-based pen tool which can overlay virtual images onto a physical drawing in real time.
    - > We also made a standalone pen system integrated with a pico-projector and a camera, and suggest a set of useful scenarios for the conventional pen-and-paper drawing.


#### Ambiente

* Gelsomini, M., G. Cosentino, M. Spitale, M. Gianotti, D. Fisicaro, G. Leonardi, F. Riccardi, et al. “Magika, a multisensory environment for play, education and inclusion”, 2019. https://doi.org/10.1145/3290607.3312753.
    - > Magika is an interactive Multisensory Environment that enables new forms of playful interventions
    - > Magika integrates digital worlds projected on the wall and the floor with a gamut of "smart" physical objects (toys, ambient lights, materials, and various connected appliances) to enable tactile, auditory, visual, and olfactory stimuli.

* Niinimäki, M., e K. Tahiroglu. “AHNE: A novel interface for spatial interaction”, 1031–34, 2012. https://doi.org/10.1145/2212776.2212378.
    - > we describe AHNE (Audio-Haptic Navigation Environment). It is a three-dimensional user interface (3D UI) for manipulating virtual sound objects with natural gestures in a real environment.
    - > AHNE uses real-time motion tracking and custom-made glove controllers as input devices, and auditory and haptic feedback as the output.

### Dispositivos / componentes (não usuais)

* “Rubikon: A highly reconfigurable device for advanced interaction”, 1327–32, 2014. https://doi.org/10.1145/2559206.2581275.
    - > Rubikon is a device that allows users to interact with common user interfaces by using an augmented Rubik's Cube that senses users' actions and displays information on each tile. Rubikon has advantages over the traditional mouse or the mid-air approach such as high number of degrees of freedom and implicit haptic feedback when rotating the sides.

#### Papel

* Chang, Z., H. Kim, K. Kato, K. Saito, T.D. Ta, W. Jiang, K. Narumi, Y. Miyamoto, e Y. Kawahara. “Kirigami keyboard: Inkjet printable paper interface with kirigami structure presenting kinesthetic feedback”, 2019. https://doi.org/10.1145/3290607.3312757.
    - dispositivo: teclado p/ touchscreen feito de papel
    - > We propose a DIY process to produce customized paper keyboards with kinesthetic feedback that interact with touchscreens.
        - > The process is built using two techniques: kirigami and printable double-layered circuits.

* Wang, G., T. Cheng, Y. Do, H. Yang, Y. Tao, J. Gu, B. An, e L. Yao. “Printed paper actuator: A low-cost reversible actuation and sensing method for shape changing interfaces”, Vol. 2018-April, 2018. https://doi.org/10.1145/3173574.3174143.

* Nebeling, M., e K. Madier. “360Proto: Making interactive virtual reality & augmented reality prototypes from paper”, 2019. https://doi.org/10.1145/3290605.3300826.


### Sensores

* Ostberg, A., M. Sheik-Nainar, e N. Matic. “Using a mobile device fingerprint sensor as a gestural input device”, 07-12-May-2016:2625–31, 2016. https://doi.org/10.1145/2851581.2892419.


### Sistemas / software

* Danyluk, K., B. Jenny, e W. Willett. “Look-from camera control for 3D terrain maps”, 2019. https://doi.org/10.1145/3290605.3300594.
    - touchscreen
    - Métodos para controlar navegação em terrenos 3D


#### Multi-device

* Chapuis, O., A. Bezerianos, e S. Frantzeskakis. “Smarties: An input system for wall display development”, 2763–72, 2014. https://doi.org/10.1145/2556288.2556956.
    - keywords: multi-device
    - > Smarties allows wall application developers to easily add interactive support to their collaborative applications.
    - > It consists of an interface running on touch mobile devices for input, a communication protocol between devices and the wall, and a library

* Houben, S., e N. Marquardt. “WATCH connect: A toolkit for prototyping smartwatch-centric cross-device applications”, 2015-April:1247–56, 2015. https://doi.org/10.1145/2702123.2702215.


### Document sharing

* Ringel, M., K. Ryall, C. Shen, C. Forlines, e F. Vernier. “Release, relocate, reorient, resize: Fluid techniques for document sharing on multi-user interactive tables”, 1441–44, 2004. https://doi.org/10.1145/985921.986085.


### Domínios de aplicação

* Hagen, K., K. Chorianopoulos, A.I. Wang, L. Jaccheri, e S. Weie. “Gameplay as exercise”, 07-12-May-2016:1872–78, 2016. https://doi.org/10.1145/2851581.2892515.
    - > We designed and evaluated an exertion video game in order to overcome two challenges that are preventing exergames from becoming a viable sustained exercise alternative; insufficient physical exertion and player retention.


### Estudos com usuários

* Hawkey, K., e K.M. Inkpen. “Keeping up appearances: Understanding the dimensions of incidental information privacy”, 2:821–30, 2006. https://dl.acm.org/citation.cfm?id=1124893
    - privacidade
    - > We conducted a survey of 155 participants to examine privacy concerns relating to the viewing of incidental information (i.e. traces of previous activity unrelated to the task at hand) in web browsers. We have identified several dimensions of privacy for this domain.

* Jota, R., P. Lopes, e J.A. Jorge. “I, the device: Observing human aversion from an HCI perspective”, 261–70, 2012. https://doi.org/10.1145/2212776.2212804.

* Wang, R., F. Quek, D. Tatar, J.K.S. Teh, e A.D. Cheok. “Keep in touch: Channel, expectation and experience”, 139–48, 2012. https://doi.org/10.1145/2207676.2207697.
    - > This paper investigates whether and how digitally mediated social touch (remote touch) may influence the sense of connectedness toward a speaker and the emotional experience of what is being communicated.

* Sun, E., e S. Han. “Fun with Bananas: Novel Inputs on Enjoyment and Task Performance”, 2013-April:1275–80, 2013. https://doi.org/10.1145/2468356.2468584.
    - novas formas de interação; makey makey
    - ...
    - > We discuss potential implications and future work around the implementation of novel input devices.

* Müller-Tomfelde, C., e C. Schremmer. “Touchers’ and ‘mousers’: Commonalities and differences in co-located collaboration with multiple input devices”, 1149–52, 2008. https://doi.org/10.1145/1357054.1357234.


### Técnicas de fabricação

* Hook, J., P. Wright, T. Nappey, P. Olivier, e S. Hodges. “Making 3D printed objects interactive using wireless accelerometers”, 1435–40, 2014. https://doi.org/10.1145/2559206.2581137.
    - > We present an approach that allows designers and others to quickly and easily make 3D printed objects interactive, without the need for hardware or software expertise and with little modification to an object's physical design.

* Laput, G., E. Brockmeyer, S.E. Hudson, e C. Harrison. “Acoustruments: Passive, acoustically-driven, interactive controls for handheld devices”, 2015-April:2161–70, 2015. https://doi.org/10.1145/2702123.2702414.
    - > We introduce Acoustruments: low-cost, passive, and power-less mechanisms, made from plastic, that can bring rich, tangible functionality to handheld devices.
    - > Through a structured exploration, we identified an expansive vocabulary of design primitives, providing building blocks for the construction of tangible interfaces utilizing smartphones' existing audio functionality.

* Li, H., E. Brockmeyer, E.J. Carter, J. Fromm, S.E. Hudson, S.N. Patel, e A. Sample. “PaperID: A technique for drawing functional battery-free wireless interfaces on paper”, 5885–96, 2016. https://doi.org/10.1145/2858036.2858249.
    - papel
