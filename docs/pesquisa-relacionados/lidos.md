# Lidos

## Relacionados

##### Weinberg et al. (2000): Musical Ball

![](../imagens/artigos/musical-balls-dispositivo.png)

![](../imagens/artigos/musical balls - uso.png)

* **Referência**:
    * Weinberg, G., M. Orth, e P. Russo. “The embroidered musical ball: A squeezable instrument for expressive performance”, 283–84, 2000. https://doi.org/10.1145/633292.633457.

* **Keywords**: - esfera - "squeezable" - controle/input

- **Proposta**:
    - > we describe the Embroidered Musical Ball, a soft, tactile computer/MIDI musical instrument, that lets untrained children, novices and/or professionals perform and manipulate expressive and detailed music with simple everyday physical hand gestures such as squeezing and stretching.

- **Problemática**:
    - > The Embroidered Musical Ball was created to allow untrained children, novices (and/or professionals) to perform and manipulate artistic music with simple everyday physical hand gestures such as squeezing and stretching.

- **Dispositivo**:
    - Uma bola de tecido com sensores de pressão
        - > stuffed fabric ball embroidered with eight continuous pressure sensors sewn from conductive thread
        - > the sensors are sewn in a circle around a soft hand held ball
    - > The soft, tactile and visually stimulating fabric material of the ball also encourages players to touch and explore it physically.

- **Arquitetura**:
    - > a multimedia desktop computer [...] takes in the sensor data from the ball and creates music.
    - > The ball contains a microprocessor, which measures the embroidered sensors capacitively
        - > and sends serial data through a wire to the desktop computer.

- **Forma de interação**:
    - Sensores de 'força':
        - > Simply holding the ball in two hands and squeezing lets a player immediately control eight channels of continuous pressure sensing.

- **Sensores**:
    - "Textile sensors"
    - > The embroidered sensors are high impedance electrodes whose change in capacitance is measure in the time domain on a programmable microprocessor, PIC 16F84. The PIC measures the change capacitance on the electrodes and send that data serially to a desktop computer


##### Perelman et al. (2015)


![](../imagens/artigos/roly-poly mouse - gestos.png)

![](../imagens/artigos/roly-poly mouse - prototipo.png)

* **Referência**:
    * Perelman, G., M. Serrano, M. Raynal, C. Picard, M. Derras, e E. Dubois. “The roly-poly mouse: Designing a rolling input device unifying 2d and 3D interaction”, 2015-April:327–36, 2015. https://doi.org/10.1145/2702123.2702244.

* **Keywords**: esfera
* **Leitura**: visão geral

- **Proposta**:
    - > We present the design and evaluation of the Roly-Poly Mouse (RPM), a rolling input device that combines the advantages of the mouse (position displacement) and of 3D devices (roll and rotation) to unify 2D and 3D interaction.
    - > Our first study explores RPM gesture amplitude and stability for different upper shapes (Hemispherical, Convex) and hand postures.
    - ...

- **Problemática**: integrar controle 2D (mouse) e 3D

- **Dispositivo**:
    - > device  mimics  the  well-known  roly-poly  toy [?] :  the  base  of the  device  is  hemispherical  and  can  be  rolled  (2DOF), rotated   (1DOF)   and   translated   (up   to   3DOF)   in   any direction
        - **Nota**: 'roly-poly toy' -> 'joão bobo'?
    - > the device rolls and auto-repositions itself when released, similar to an isometric device [5]. The hemispherical bottom offers a large curved surface allowing for a wide range of possible input values. The device is symmetrical along its rotation axis (z) and thus can be used in any orientation.

    - **Forma física**:
        - > size of RPM is based on the average size of a regular mouse (approx. 12x6cm).
        - > Focusing on a symmetrical form that a hand would comfortably hold, we opted for a hemisphere and varied its degree of curvature and direction (in and out).
            - > In our preliminary study we initially consider three upper shapes: hemispherical, convex (curved out) and concave (curved in).

- **Arquitetura**:
    -

- **Forma de interação**:
    - Dispositivo é utilizado sobre uma superfície (como um mouse) e permite: translação 2D (como um mouse); rotacionado sobre eixo vertical (yaw); inclinado para 'esquerda' ou 'direita' (roll); ou inclinado para 'frente' e para 'trás' (pitch);
        - movimentos podem ser combinados
    - > The device offers up to 6 DOF with three types of simple gesture (Figure 1): translation (3 DOF), roll (2 DOF: pitch and roll) and rotation (1 DOF: yaw).
        - > In the context of desktop usage, we don’t consider the z-axis translation due to fatigue [...]
    - > The three simple gestures (translation, rolling and rotation) can be combined to create compound gestures.

- **Sensores e componentes**:
    - >  To  detect  the  3D  displacement  and rotation  of  RPM,  we  used  a  Polhemus  Patriot  Wireless tracker  (7x3x2.5cm,  79.4gr.).
    - > ‘ring’  button  all around  the  device.
        - > the button consists of one resistive potentiometer (81x7.5x0.5mm) that provides up to 1024 values depending on the touch position.
            - > It is thus possible to combine the clicked position on the ring with the current RPM orientation to support multiple buttons.
    - > To interface it, we used an Arduino Fio board with a Bluetooth shield and an external battery (Figure 11).

- **Relação**:
    - Artigo apresenta um dispositivo de interação/controle esférico
    - Artigo inclui também considerações interessantes sobre tamanho do dispositivo e posições para segurá-lo.
    - Protótipo é interessante também para pesquisa
    - Talvez "gestos" para uso do dispositivo possam ser úteis também
    - Dispositivo apresentado é projetado para uso sobre uma superfície (e não de forma livre)

--------------------------------------------------------------------------------------------------------------

## Parcialmente relacionados


### Técnicas de interação

##### Keränen et al. (2009): Gravity sphere

* **Referência**:
    * * Keränen, J., J. Bergman, e J. Kauko. “Gravity sphere: Gestural audio-tactile interface for mobile music exploration”, 1531–34, 2009. https://doi.org/10.1145/1518701.1518935.

* **Keywords**: - esfera - controle (?)

* Leitura: **Visão Geral**

- **Proposta**:
    - >  we  describe  a  system  for  exploring  a  music  library  and  quickly  generating  playlists  in  mobile  contexts.
    - > audio-tactile feedback and is controlled by manipulating a device's orientation.

- **Problemática**:
    - >  Browsing [Music  libraries] such libraries using portable devices is inconvenient due to the  restrictions  of  mobile user interfaces.

- **Dispositivo**:
    - Dispositivo móvel (smartphone, tablet, ...)

- **Forma de interação**:
    - Utiliza rotação do dispositivo como forma de interação
        - > Songs in the music library are mapped to different orientations. Songs are placed onto the surface of a virtual sphere that has the device in its center.
        - > This spherical map of songs rotates with the device – the device is a physical handle for rotating  the  sphere.
            - > The  system  then  picks  songs  from  the  spot  on  the  sphere  at  which  the  gravity  vector  points.

- **Sensores**:
    - > An accelerometer is used to sense the direction of gravity in the  device’s  local  frame  of  reference.
        - > Gravity  Sphere  uses  this  information  to  determine  the  orientation  of  the  device.

- **Relação**:
    - Forma de interação (baseada na rotação) pode ser adaptada para esferas


##### Roudaut et al. (2011)

![](../imagens/artigos/touch curved surfaces - findings.png)

![](../imagens/artigos/touch curved surfaces - uso.png)

* **Referência**:
    * Roudaut, A., H. Pohl, e P. Baudisch. “Touch input on curved surfaces”, 1011–20, 2011. https://doi.org/10.1145/1978942.1979094.

* **Keywords**: técnica, touch, esfera
* **Leitura**: visão geral
* **Tipo de trabalho**: estudo com usuários

- **Problemática**:
    - > [se tornou possível] touch input to non-planar surfaces, ranging from spherical touch screens to prototypes the size and shape of a ping-pong ball.
    - > [...] question arises of how to design usable interfaces for them [dispositivos com superfícies 'não-planares'].
        - > Unfortunately, there is no empirical data about the human factors of touch on curved surfaces yet.

- **Proposta**:
    - > we select a tractable, self-contained subset of variables, namely, single touch on spherical shapes, as these already fit existing devices.
    - > We present a user study in which participants acquired targets on surfaces of different curvature and at locations of different slope.

- **Contribuições**:
    - > We report how surface curvature affects pointing accuracy
    - > We provide minimum button sizes to help interface designers find the best location for their controls on a curved surface.
    - > We also report systematic error offsets that allow engineers to increase the accuracy of their devices by compensating for them [13].

- **Resultados**:
    - > Surface convexity increases pointing accuracy and (b) concave surfaces are subject to larger error offsets.
        - > This is likely caused by how concave surfaces hug the user’s finger thus resulting in a larger contact area.
    - > When acquiring targets on a downhill slope participants employ a hooked finger gesture, which helps them target more effectively.
    - > Our findings indicate that the curvature of touch surfaces impacts targeting in terms of spread/minimum button size and in particular in terms of systematic offsets.
        - > We found that error offsets depend on curvature (H1) and slope (Q1).
        - > effects of curvature on spread, in contrast, are moderate.
            - > There appears to be an effect for convex targets. For concave targets in contrast, multiple confounding factors compensate for each other.
    - > Our findings suggest placing targets on points of extreme curvatures in order to make them easier to acquire, so application designers might want to use them for frequently used functions.

- **Sensores e componentes**:
    - Autores utilizam 'FTIR' (uma técnica óptica) para detectar toques
    - A superfície de contato é construída utilizando acrílico

- **Relação com a pesquisa**:
    - Ver relacionados do artigo p/ tecnologias e *"non-planar touch screens, such as Sphere [3]."*
    - Diferença: estudo (aparentemente) foca em uso de superfícies não planas como touchscreens. "


### Tecnologias de interação

##### Rosenberg et al. (2009): IMPAD

![](../imagens/artigos/IMPAD-uso.png)

![](../imagens/artigos/IMPAD-layers.png)

* **Referência**:
    * Rosenberg, I., A. Grau, C. Hendee, N. Awad, e K. Perlin. “IMPAD - An inexpensive multi-touch Pressure Acquisition Device”, 3217–22, 2009. https://doi.org/10.1145/1520340.1520460.

* **Keywords**:
* **Leitura**: visão geral
* **Tipo de trabalho**: tecnologia

- **Problemática**:
    -

- **Proposta**:
    - > We have created a technology that enables the creation of Inexpensive Multi-Touch Pressure Acquisition Devices (IMPAD) which are paper-thin, flexible and can easily scale down to fit on a portable device or scale up to cover an entire table.
    - > These devices can sense varying levels of pressure at a resolution high enough to sense and distinguish multiple fingertips, the tip of a pen or pencil and other objects.


- **Dispositivo**:
    - > We designed the UnMousePad as a stand-alone multi-touch input pad for use on the surface of a desk while looking ahead at a computer screen.
        - > Because it looked a bit like a mouse-pad but didn’t require a mouse to operate, we affectionately called it the UnMousePad
    - > because the UnMousePad does not rely on capacitance, it can detect pens, pencils, styli and other inanimate objects just as well as it detects fingers.
    - > the UnMousePad is flexible (able to be wrapped around curved surfaces), robust and portable, can be used with existing computers and displays and would have a cost similar to a keyboard or mouse when produced in bulk.

- **Arquitetura**:
    -

- **Forma de interação**:
    -

- **Sensores e componentes**:
    - IMPAD:
        - > IMPAD more closely mimics the multi-resolution properties of human skin, in which the position of a touch can be detected at finer scale than the discrimination of multiple touches
        - > accomplished by using arrays of sensors having overlapping fields of influence which fall off with distance.
            - > spatially variant pressure is detected through a bilinear filter
        - > an IMPAD sensor conceptually consists of five layers (Figure 7):
            - > the first and fifth (outer) layers consist of parallel wires which allow interface with external electronics.
                - > The parallel wires on layer 1 run in a direction perpendicular to the direction of the parallel wires on layer 5.
            - > The second and fourth layers consist of resistive material which allows a trickle of current between adjacent wires and accounts for the bilinear nature of the device.
            - > The third (middle) layer consists of a material whose electrical resistance decreases when compressed allowing for the sensing of outside forces.
        - > During operation,
            - > one wire at a time along layer 1 is sourced to a positive voltage while all the others are set to ground.
            - > Then, the voltage at each of the wires on layer 5 is measured one at a time as all the other wires on layer 5 are set to ground. This repeats until a voltage reading is taken for each intersection between a row and a column wire.
            - > it is also possible to read voltages from all the even numbered wires simultaneously followed by all the odd numbered wires.
        - > When external pressure is applied to the sensor, a path is created that allows current to flow from layer 1 to layer 5.
            - > the resulting voltage at an intersection depends on the resistance of that current path which depends on the proximity of the contact point from the intersection and the pressure exerted at that point.

- **Relação com a pesquisa**:
    - O artigo apresenta uma tecnologia que poderia ser utilizada para detectar toque/pressão na superfície da esfera
        - Entretanto não seria (provavelmente) viável construí-la manualmente (em um curto período de tempo)



-------------------------------------------------------------------------------------------------------------

## Talvez relacionados

### Esferas

##### Bolton et al. (2011)


![](../imagens/artigos/snow-globe-uso.png)

* **Referência**:
    * Bolton, J., K. Kim, e R. Vertegaal. “SnowGlobe: A spherical fish-tank VR display”, 1159–64, 2011. https://doi.org/10.1145/1979742.1979719.

* **Keywords**: - esfera - display - touch

* Leitura: **Visão geral**

- **Proposta**:
    - > we present a spherical display with Fish-Tank VR as a means for interacting with three-dimensional objects.

- **Problemática**:

- **Dispositivo**:
    - Uma esfera + sistema de projeção + "câmeras" (?)


- **Forma de interação**:
    - gestual + "toques":
        - Sistema detecta toque na esfera e gestos
    - Gestos:
        - Pinching:
            - > Scaling is performed as a bimanual gesture. By placing two touches on the spherical screen the user can uniformly scale the model, using a pinching gesture.
        - Rotação:
            - > By touching the sphere at one point and moving along an imaginary line of latitude the model will be rotated along the y axis. By placing one touch on the surface and moving along a longitude, the model will be rotated along its x axis. This gesture is based on the concept of the user spinning the sphere as if it was a free-spinning globe.
        - Swiping:
            - > A user can change the current model by using a full handed scroll gesture of sufficient speed. By placing a hand on the display surface and quickly moving it along a latitude in a clockwise manner the user can navigate through available models.

- **Sensores**:
    - > Diffuse illumination is used for detecting touch points on the sphere.
    - > the user’s head position is tracked using a Vicon motion capture system. This allows our system to preserve motion parallax and present the correct 3D perspective of 3D objects when walking around the display.


### Técnicas


#### Gestos + 'grasped objects'

##### Wolf, K. (2012)

* **Referência**:
    * Wolf, K. “When hand and device melt into a unit. Microgestures on grasped objects”, 959–62, 2012. https://doi.org/10.1145/2212776.2212875.

* **Keywords**:
* **Leitura**: visão geral
* **Tipo de trabalho**: Proposta de pesquisa

- **Proposta**:
    - > My approach aims to apply mental models for designing microinteractions (microgestures, feedback, and applications) while grasping.
    - > Here I will focus on humans’ motor abilities, interaction favors, and the mental model of themselves within their environment.

- **Problemática**:
    - > I aim to investigate the possibility of the human hand to be a magical tool through fusing it with the technical side of the interface.
    - > Attaching sensors to fingers allows not just free spatial gestures but also (because of the incredible motor and sensory abilities of our hand) performing tiny finger movements while grasping daily objects.
    - > interaction is established when grasping an object. Sensors are attached to the fingers, but the semantics of the finger gestures are established only in the moment of grasping the object.

    - > Sensing tiny finger movements is a current topic of the related work. Other problems, such as the question, which objects or tasks benefit from microinteractions, how should microgestures be designed, and how can this interaction be supported (through affordances, constraints, guidance, and feedback) are challenging, so far not answered


- **Relação com a pesquisa**:
    - Ver artigos relacionados (referenciados ou posteriores) sobre tópico gestures + 'grasped objects' pode prover técnicas p/ utilizar com esfera

--------------------------------------------------------------------------------------------------------------


#####

* **Referência**:
    *

* **Keywords**:
* **Leitura**:
* **Tipo de trabalho**:

- **Problemática**:
-
- **Proposta**:
    -


- **Dispositivo**:
    -

- **Arquitetura**:
    -

- **Forma de interação**:
    -

- **Sensores e componentes**:
    -


- **Relação com a pesquisa**:
    -
