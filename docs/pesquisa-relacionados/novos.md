# Novos

## Relacionados

### Weinberg et al. (2000): Musical Ball

* Boem, A., e G.M. Troiano. “Non-Rigid HCI: A review of deformable interfaces and input”, 885–906, 2019. https://doi.org/10.1145/3322276.3322347.

* Fishkin, K.P. “A taxonomy for and analysis of tangible interfaces”. Personal and Ubiquitous Computing 8, nº 5 (2004): 347–58. https://doi.org/10.1007/s00779-004-0297-4.

* Nitta, K., T. Sato, H. Koike, e T. Nojima. “PhotoelasticBall: A touch detectable ball using photoelasticity”, 2014. https://doi.org/10.1145/2582051.2582067.

* Sugiura, Y., G. Kakehi, A. Withana, C. Lee, D. Sakamoto, M. Sugimoto, M. Inami, e T. Igarashi. “Detecting shape deformation of soft objects using directional photoreflectivity measurement”, 509–16, 2011. https://doi.org/10.1145/2047196.2047263.

* Van Nort, D., D. Gauthier, S.X. Wei, e M.M. Wanderley. “Extraction of gestural meaning from a fabric-based instrument”, 441–44, 2007.

* Vandenberghe, B., K. Gerling, L. Geurts, e V.V. Abeele. “Skweezee for processing: A software library to make squeeze interactions”, 375–81, 2019. https://doi.org/10.1145/3294109.3300984.


### Wolf K.

* Wolf, Katrin. “Ubiquitous Grasp Interfaces”. In Proceedings of the 7th International Conference on Tangible, Embedded and Embodied Interaction, 377–378. TEI ’13. New York, NY, USA: ACM, 2013. https://doi.org/10.1145/2460625.2460702.

* Wolf, K. “What i grasp is what i control: Interacting through grasp releases”, 389–90, 2012. https://doi.org/10.1145/2148131.2148228.

* Wolf, K., M. McGee-Lennon, e S. Brewster. “A study of on-device gestures”, 11–16, 2012. https://doi.org/10.1145/2371664.2371669.

* Wolf, K., R. Schleicher, S. Kratz, e M. Rohs. “Tickle: A surface-independent interaction technique for grasp interfaces”, 185–92, 2013. https://doi.org/10.1145/2460625.2460654.

* Wolf, K., e J. Willaredt. “PickRing: Seamless interaction through pick-up detection”, 11:13–20, 2015. https://doi.org/10.1145/2735711.2735792.
