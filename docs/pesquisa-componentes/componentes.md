# Componentes - Esfera Interativa

## Pesquisa de Materiais|componentes

* **Sensores**
    - Movimento/Rotação: Acelerômetro/Giroscópio/Magnetômetro(?)
        - [mpu-6050][giroscopio-mpu6050-filipeflop] - R$17
        - [Filipe Flop][filipeflop-giroscopio]
        - [Adafruit][adafruit-motion]
    - Aperto/Pressão: Sensor de força (?)
        - [Filipe Flop - sensores de toque][filipeflop-toque]
        - [Adafruit - sensores de toque][adafruit-toque]
        - [DIY - FSR 1][instructables-diy-fsr]
        - [DIY - FSR 2][instructables-diy-cheap-fsr]
        - [40 cent DIY pressure sensor based on a capacitive principle](https://blog.yavilevich.com/2017/10/40-cent-diy-pressure-sensor-based-on-a-capacitive-principle/)
        - [Video - Pressure Sensor DIY](https://www.youtube.com/watch?v=-ZVcvO-UqRs)
        - materiais:
            - [Goophene: DIY Hypersensitive Graphene Sensors](https://hackaday.io/project/38953-goophene-diy-hypersensitive-graphene-sensors)
            - silicone condutivo - [Silc Circuits: High Performance Conductive Silicone](https://www.instructables.com/id/Silc-Circuits-High-Performance-Conductive-Silicone/)
            - [Make Conductive Rubber: Transparent Stylus-iPod/iPhone](https://www.instructables.com/id/Make-Conductive-Rubber-Transparent-stylus-iPodiP/)
    - Multiplexador p/ matriz de sensores
        - [Módulo Multiplexador AD 16 Canais CD74HC4067][multiplexador-16-filipeflop]
            - analógico/digital
            - multiplexador e demultiplexador
            - 16 portas I/O
            - 4 portas - seleção
            - Preço: R$14
        - [Módulo Multiplexador 8 Canais 74HC4051 Sparkfun](https://www.filipeflop.com/produto/modulo-multiplexador-8-canais-74hc4051-sparkfun/)
            - analógico/digital
            - multiplexador e demultiplexador
            - 8 portas I/O
            - 3 portas - seleção
            - Preço: R$13

* **Atuadores**
    - Vibração: "haptic motor" (?)
        - [Filipe Flop][filipeflop-vibracao]
    - Som: buzzer
    - Luz: LED RGB (?)
        - LED RGB 4 pinos
            - [filipeflop](https://www.filipeflop.com/produto/led-rgb-alto-brilho-difuso-5mm/) - R$ 2,40 (unidade)
        - LED RGB endereçável (WS2812)
            - [2 blocos de 5 leds]() - R$ 20 (10 leds) -> R$ 2,0 (unidade)
            - [fita de led - filipeflop][leds-end-rgb-10-filipeflop] - R$ 55 (30 leds) -> R$ 1,85 (unidade)

* **Energia**
    - Bateria
        - Li-Po
            - Exemplo: [Bateria Li-Po 400mAh Conector 51021][bateria-li-po-400-filipeflop]
                - Tensão Nominal: 3,7 V
                - Capacidade: 400 mAh
                - Dimensões: 35 mm x 19 mm x 6 mm
                - Peso: 8 g
                - preço: R$40
        - Li-ion
            - Exemplo: [Bateria Li-Íon 18500 1500 mAh][bateria-li-on-1500-filipeflop]
                - Capacidade: 1500mAh
                - Tamanho: 49mm (comprimento); 18mm (diâmetro)
                - peso: 45g
                - Tensão Nominal: 3,7 V
                - preço: R$37
        - [comparação entre tipos](https://blog.ravpower.com/2017/06/lithium-ion-vs-lithium-polymer-batteries/)
    - Placa de alimentação
        - Exemplo: [Módulo Carregador de Bateria de Lítio TP4056][carregador-bateria-litio-filipeflop]
    - Conectores p/ alimentação
        - Indutivo?
        - USB (mini, micro, C)
    - Regulador de tensão (p/ bateria)
        - Step down:
            - normalmente requerem uma diferença de tensão entre entrada e saída relativamente alta (e.g. > 1.5v)
        - LDO (Low Dropout)
        - Step up

* **Processamento**
    - Placa de prototipagem
        - ESP32
            - Inclui Bluetooth, Wi-Fi e pinos com suporte a touch capacitivo
            - [FilipeFlop][esp32-filipeflop] - R$70
        - NodeMCU
        - Arduino

* **Conectividade**
    - Bluetooth (?)
    - Wi-Fi (?)

## Seleção de componentes

##### Módulos e componentes principais

Classificação | Tipo componente  |  Escolha | Custo
--- | --- | --- | ---
Proc. + Conect. | Placa                   | [ESP32][esp32-filipeflop] | R$70
                | (alternativa)           | NodeMCU + Módulo Bluetooth |
Sensores        | giroscópio/acelerômetro | [mpu-6050][giroscopio-mpu6050-filipeflop] | R$20
Atuadores       | Motor háptico | [Motor de Vibração 1027](https://www.filipeflop.com/produto/motor-de-vibracao-1027/) | R$10
                | (alternativa) | Extrair (de celular) |
Atuadores       | Leds RGB      | [10 leds rgb endereçáveis][leds-end-rgb-10-filipeflop] | R$27
                | Faixa de Leds RGB (alternativa) | [30 leds rgb endereçáveis](https://www.filipeflop.com/produto/fita-de-led-rgb-ws2812-5050-1m/) | R$55
Alimentação     | Bateria               | [Bateria Li-Íon 18500 1500 mAh][bateria-li-on-1500-filipeflop]  | R$37
                | (alternativa)      | Extrair (bateria notebook)  |
Alimentação     | Carregador de bateria | [Módulo Carregador de Bateria de Lítio TP4056][carregador-bateria-litio-filipeflop] | R$10
Alimentação     | Regulador de tensão p/ bateria | [Conversor Boost DC Ajustável Step Up](https://www.filipeflop.com/produto/conversor-boost-dc-step-up/) | R$10


##### Outros componentes

Classificação | Tipo componente  |  Escolha | Qtd. | Custo
--- | --- | --- | --- | ---
Atuadores   | Buzzer 5V   | [Buzzer][buzzer-filipeflop]  | 1 | R$4
Auxiliares  | Multiplexador | [Módulo Multiplexador AD 16 Canais CD74HC4067][multiplexador-16-filipeflop]   | 1 | R$14
Auxiliares  | Switch on/off| [Chave 3 Terminais Mini Switch SPDT][mini-switch-filipeflop] | 1 | R$ 1,4
Auxiliares  | Transistores | ? | ? | ?
Auxiliares  | Conectores   | ? | ? | ?


## Referências

* [Nintendo switch teardown][ifixit-switch-teardown]

* Placa:
    * [ESP32 Pinout](https://randomnerdtutorials.com/esp32-pinout-reference-gpios/)
* Alimentação:
    * [Power ESP32/ESP8266 with Solar Panels (includes battery level monitoring)](https://randomnerdtutorials.com/power-esp32-esp8266-solar-panels-battery-level-monitoring/)
    * [ESP8266 Voltage Regulator (LiPo and Li-ion Batteries)] (https://randomnerdtutorials.com/esp8266-voltage-regulator-lipo-and-li-ion-batteries/)
        - Utiliza LDO (MCP1700-3302E)
    * [Battery-Powered ESP32](https://www.radioshuttle.de/en/media-en/tech-infos-en/battery-powered-esp32/)


[Links]: **************************************************************************************************

[ifixit-switch-teardown]: ../Nintendo%20Switch%20Teardown%20-%20iFixit%20(2019-10-08%2011_47_48).html

[giroscopio-mpu6050-filipeflop]: ../acelerometro-giroscopio-MPU-6050-filipeflop-2019-11-07_15-09-12.html
[filipeflop-giroscopio]: ../pesquisa%20girosc%C3%B3pio%20-%20FilipeFlop%20(2019-10-08%2011_26_29).html
[adafruit-motion]: ../motion%20sensors%20-%20adafruit%20(2019-10-08%2011_41_39).html

[filipeflop-toque]: ../sensores%20toque%20-%20filipeflop%20(2019-10-08%2011_44_49).html
[adafruit-toque]: ../touch%20sensors%20-%20adafruit.html
[instructables-diy-fsr]: ../DIY%20FSR%20-%20instructables.html
[instructables-diy-cheap-fsr]: ../DIY%20FSR%20-%20cheap%20analog%20pressure%20sensor%20-%20instructables.html

[filipeflop-vibracao]: ../filipeflop%20-%20motor%20de%20vibra%C3%A7%C3%A3o%20-%20(2019-10-08%2012_05_28).html

[esp32-filipeflop]: ../ESP32-filipeflop-2019-11-07_14-20-12.html


[leds-end-rgb-10-filipeflop]: https://www.filipeflop.com/produto/led-rgb-x10-ws2812b-5050-enderecavel/

[buzzer-filipeflop]: https://www.filipeflop.com/produto/buzzer-ativo-5v/
[mini-switch-filipeflop]: https://www.filipeflop.com/produto/chave-3-terminais-mini-switch-spdt/
[multiplexador-16-filipeflop]: https://www.filipeflop.com/produto/modulo-multiplexador-ad-16-canais-cd74hc4067/

[Alimentação]: **********************************************************************

[bateria-li-on-1500-filipeflop]: https://www.filipeflop.com/produto/bateria-li-ion-18500-1500-mah/
[bateria-li-po-400-filipeflop]: https://www.filipeflop.com/produto/bateria-li-po-400mah-conector-51021/
[carregador-bateria-litio-filipeflop]: https://www.filipeflop.com/produto/modulo-carregador-de-bateria-de-litio-tp4056/)
