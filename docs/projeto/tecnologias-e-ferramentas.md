# Tecnologias e Ferramentas

## Projeto da Esfera

### Visão Geral

![Projeto de Hardware Geral](../imagens/projeto/projeto-de-hw__principal.jpg)


### 'Carcaça'

#### Partes

- Superfície
    - `Função`: proteger materiais internos; oferecer superfície de toque para usuário; estética da esfera;
    - `Requisitos`: ...
- Camada flexível
    - `Função`: Permitir deformação ao pressionar esfera
    - `Requisitos`: ...
- Esfera interna
    - `Funções`:
        - Estrutura base da esfera
        - Base para camadas exteriores
        - Suporte para parte interior (circuitos)
    - `Requisitos`:
        - Permitir abertura
        - ...
- Base interna para circuitos
    - `Função`: Superfície em que componentes/circuitos principais serão fixados
    - `Requisitos`: ...
- Base (externa) da esfera
    - `Funções`:
        - Superfície de apoio para manter esfera estável sobre superfícies
        - Expor conectores
    - `Requisitos`: ...

#### Materiais e Projeto

- Superfície + Camada Flexível
    - `Materiais`:
        - Silicone
            - Permite colorir e moldagem
            - É um material flexível e resistente (?)
    - `Projeto`: ...
    - `Questões`: ...
- Esfera interna
    - `Materiais`:
        - PET
            - Barato e fácil de encontrar
            - Pode ser moldado (aquecimento)
            - Talvez seja frágil
        - Acrílico
            - Mais resistente
        - PVC
        - ...?
    - `Projeto`: ...
    - `Questões`: ...
- Base interna para circuitos
    - `Materiais`:
    - `Projeto`: ...
    - `Questões`: ...
- Superfície (inferior) de apoio
    - `Materiais`:
    - `Projeto`: ...
    - `Questões`: ...


---------------------------------------------------------------------------------------------

### Eletrônica


![Módulos de Hardware](../imagens/projeto/modulos-hardware.jpg)

#### Componentes | Módulos

* Sensores de força
    - 'Paper skin'
        - Materiais: Papel alumínio + papel
        - Referências:
            - [Artigo -  Paper Skin Multisensory Platform for Simultaneous Environmental Monitoring](../resources/artigos/paper-skin-multisensory-platform-for-simultaneous-environmental-monitoring_2016.pdf)
        - Obs.: não fica claro como circuito + código podem ser construídos
    - MultiTouchKit
        - Limitações:
            - utilizado para detectar 'multitouch'
            - precisaria adaptar (se for possível) para detectar pressão
            - pode precisar adaptar código para ESP32
        - Referências:
            - [artigo](../resources/artigos/Multi-Touch_Kit_UIST19.pdf)
            - [tutorial](../resources/documentos/MTK_Tutorial.pdf)


---------------------------------------------------------------------------------------------

### Software

![Módulos de Software](../imagens/projeto/modulos-software.jpg)

#### Ferramentas

* PlatformIO
    - Home: https://platformio.org/
    - docs: https://docs.platformio.org/en/latest/index.html
    - Libraries: https://platformio.org/lib
    - IDE:
        - Atom: https://docs.platformio.org/en/latest/ide/atom.html
        - QTCreator: https://docs.platformio.org/page/ide/qtcreator.html
        - VSCode: https://docs.platformio.org/page/ide/vscode.html

#### Bibliotecas e Referências

* **Esfera**
    * Dispositivos
        * Bluetooth
            - Tutoriais:
                - PlatformIO + ESP32 + Bluetooth: https://docs.platformio.org/en/latest/tutorials/espressif32/arduino_debugging_unit_testing.html#adding-bluetooth-le-features
                - [FilipeFlop - Bluetooth Low Energy com ESP32 e DHT11](https://www.filipeflop.com/blog/bluetooth-low-energy-com-esp32-e-dht11/)
        * Matriz de Sensores capacitivos
            - Bibliotecas:
                - Multi-Touch Kit
                    - > A Do-It-Yourself Technique for Capacitive Multi-Touch Sensing Using a Commodity Microcontroller
                        - Obs: precisa adaptar p/ ESP32 (?) e para sensor de força capacitivo (?!)
                    - [home](https://hci.cs.uni-saarland.de/multi-touch-kit/)
                    - [Arduino Library - github](https://github.com/HCI-Lab-Saarland/MultiTouchKit)
                    - [Docs - github](https://github.com/HCI-Lab-Saarland/MultiTouchKitDoc)
        * Acelerômetro/Giroscópio (mpu-6050)
            - Bibliotecas:
                - MPU6050_tockn: https://platformio.org/lib/show/2824/MPU6050_tockn
                    - > Arduino library for easy communicating with the MPU6050.
                - TinyMPU6050: https://platformio.org/lib/show/6180/TinyMPU6050
                    - > Tiny implementation for MPU6050 focusing on performance and accuracy
                - Adafruit MPU6050: https://platformio.org/lib/show/6782/Adafruit%20MPU6050
                    - > Arduino library for the MPU6050 sensors in the Adafruit shop
                - esp32-MPU-driver: https://github.com/natanaeljr/esp32-MPU-driver
            - Tutoriais:
                - [Arduino and MPU6050 Accelerometer and Gyroscope Tutorial](https://futurelab3d.com/arduino-and-mpu6050-accelerometer-and-gyroscope-tutorial/)
                - [Arduino - MPU6050 GY521 - 6 Axis Accelerometer + Gyro (3D Simulation With Processing)](https://www.instructables.com/id/Arduino-MPU6050-GY521-6-Axis-Accelerometer-Gyro-3D/)
        * LEDs RGB (ws2812)
            - `Adafruit NeoPixel`
                - https://github.com/adafruit/Adafruit_NeoPixel
            - Tutoriais:
                - https://www.filipeflop.com/blog/pisca-pisca-com-arduino/
    * Testes
        - `PIO Unit Testing`
            - https://docs.platformio.org/en/latest/plus/unit-testing.html
