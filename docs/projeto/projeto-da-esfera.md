# Projeto da esfera

## Arquitetura do código

```plantuml
skinparam defaultFontName Monospaced
skinparam linetype ortho
hide empty fields
hide empty methods
hide circle

class Stream
class BluetoothSerial

class SphereApi

package apis{

class LedApi
class OrientationApi
class PressureApi
class SoundApi
class VibrationApi
}

class SphereServices

package services{

class LedServices
class OrientationServices
class PressureServices
class SoundServices
class VibrationServices
}

package hardware{
    class NeopixelLedServices
    class GyroscopeServices
    class CapPressureServices
    class BuzzerSoundServices
    class VibrationMotor
}

Stream <.l. SphereApi
Stream .d.> BluetoothSerial

SphereApi .d.> apis
SphereApi --r--> SphereServices

LedApi -d[hidden]-> OrientationApi
OrientationApi -d[hidden]->  PressureApi
PressureApi -d[hidden]->  SoundApi
SoundApi -d[hidden]->  VibrationApi

LedApi .r.> LedServices
OrientationApi .r.>  OrientationServices
PressureApi .r.>  PressureServices
SoundApi .r.>  SoundServices
VibrationApi .r.> VibrationServices

SphereServices .d.> services

LedServices -d[hidden]-> OrientationServices
OrientationServices -d[hidden]->  PressureServices
PressureServices -d[hidden]->  SoundServices
SoundServices -d[hidden]->  VibrationServices


LedServices <|-r- NeopixelLedServices
OrientationServices <|-r-  GyroscopeServices
PressureServices <|-r-  CapPressureServices
SoundServices <|-r-  BuzzerSoundServices
VibrationServices <|-r- VibrationMotor

NeopixelLedServices -d[hidden]-> GyroscopeServices
GyroscopeServices -d[hidden]->  CapPressureServices
CapPressureServices -d[hidden]->  BuzzerSoundServices
BuzzerSoundServices -d[hidden]-> VibrationMotor

```
